This folder contains the script (main.py) that is in charge of launching multiple instances of the script that controls the PN532 reader. It contains also a simulator (reader_simulator.py) for that script to make it possible to test it without the need of the read hardware (even though using the real script without the hardware cause no issues, then reader_simulator.py is indeed useless and will be probably removed).

It works with python3 and it needs some packages that are not installed by default.
To install them:
 python3 -m pip install paho-mqtt
 python3 -m pip install pyserial
 python3 -m pip install pexpect
 python3 -m pip install Adafruit_BBIO

The main.py script starts as many processes as the length of the variable READERS_DEF which defines the uart ports where the readers are connected, the output of each of all the processes is printed on screen. The input is instead passed to all the processes.

NOTE: for now the definitions of the readers' port is hardcoded in the script (in READERS_DEF variable). Then to edit the readers definition the script must be edited.

The script can be invoched with:

    python3 main.py ../../PN532/python_PN532/main.py iot.smartcommunitylab.it 1883 l4L4kUP9SRDQiXxAlh8r v1/devices/me/telemetry

Where:
python3 main.py                     : is the basic comand to launch the python interpreter and run the script
../../PN532/python_PN532/main.py    : is the path to the script that will be launched. Default value: reader_simulator.py
iot.smartcommunitylab.it            : is the address of the mqtt broker
1883                                : is the port of the broker
l4L4kUP9SRDQiXxAlh8r                : is the device token to use for mqtt publish
v1/devices/me/telemetry             : is the device topic to use for mqtt publish

Note that the order of arguments cannot change, it is possible to remove arguments at the end, but not in the middle.

The script can also be called with no arguments. It will run the simulator code with default parameters (those used as example above). By pressing "p+enter" all the readers will publish dummy data to mqtt
To use Neopixel leds the script needs to be launched with root permissions, otherwise it won't be able to control them.
