import json

frames=[]

C_INC=16
NEOPIXEL_LEN=12
ANIMATION_LEN=NEOPIXEL_LEN*(256//C_INC)*3
FRAME_INTERVAL=1/18

for frameIdx in range(0,ANIMATION_LEN):
    leds=[]
    frame=dict()
    [leds.append(dict()) for _ in range(NEOPIXEL_LEN)]
    for arrayIdx in range(0,NEOPIXEL_LEN*3):
        if arrayIdx<((frameIdx*C_INC)//256):
            val=255
        if arrayIdx==((frameIdx*C_INC)//256):
            val=(frameIdx*C_INC)%256
        if arrayIdx>((frameIdx*C_INC)//256):
            val=0
        
        colIdx=arrayIdx//NEOPIXEL_LEN
        if colIdx==0:
            color='r'
        elif colIdx==1:
            color='g'
        else:
            color='b'

        #print(str(colIdx)+" "+color)

        ledIdx=arrayIdx%NEOPIXEL_LEN
        leds[ledIdx][color]=val
        frame['leds']=leds
    print(str(frame))
    frames.append(frame)

print("The animation contains "+str(len(frames))+" frames reproduced at "+str(1/FRAME_INTERVAL)+"fps. It will last: "+str(len(frames)*FRAME_INTERVAL)+"s")

message=dict()
message['frames']=frames
message['frameInterval']=FRAME_INTERVAL
message['append']=False
message['repeat']=-1
#print(str(video))
jdata=json.dumps(message)

f=open("message_dump.txt", "w")
f.write(jdata)
f.close

print("File created!")
