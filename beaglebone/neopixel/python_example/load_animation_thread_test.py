import pexpect
import time
import sys
import json
from collections import deque
import threading
import queue

DEFAULT_ANIMATION_FILENAME="message_dump.txt"
DIM_PERCENT=10

if len(sys.argv)>1:
    animation_filename=sys.argv[1]
else:
    animation_filename=DEFAULT_ANIMATION_FILENAME

positive_neopixel_response="waiting for response from pru0... OK\r\n"
def initialize_neopixel():
    m_led_process=pexpect.spawnu("./../bin/neopixel_set_rgb 12 ./../bin/ws281x.bin",echo=False)
    m_led_process.expect(positive_neopixel_response,timeout=10)
    #print(m_led_process.before+positive_neopixel_response[:-2])
    init_ok=True
    return m_led_process

class NEOPIXEL_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True
        self.initialized=False

    def run(self):
        global neopixel_queue
        f=0
        #NOTE: the neopixel program always fail when relaunched. However it always work if at least two attempts are made.
        # The next section tries at most two times
        try:
            led_process=initialize_neopixel()
            self.initialized=True
            print("initialized 1!")
        except pexpect.exceptions.EOF as e:
            #print("Exception during neopixel initialization. It is typical to get this message at least once. The programm will retry...")
            time.sleep(0.1)
            led_process=initialize_neopixel()
            self.initialized=True
            print("initialized 2!")
        
        while self.__loop:
            try:
                running_animation=neopixel_queue.popleft()
                frames=running_animation['frames']
                frame_interval=running_animation['frameInterval']
                #print(str(frame_interval*1000))
                append=running_animation['append']
                repeatTimes=running_animation['repeat']
                frameIdx=0
                init_time=time.time()
                animation_len=len(frames)
                print("The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps. It will last: "+str(animation_len*frame_interval)+"s")
                while frameIdx<(animation_len*repeatTimes) or repeatTimes==-1:
                    try:
                        new_animation=neopixel_queue[-1] #do not pop the new animation, just check for the append field. 
                        if not new_animation['append'] or repeatTimes==-1: #if the new animation has to overwrite the running one then stop it, the new one will be poped at the next iteration on the thread. If the running animation is configured to run to infinite, any new animation (regardless the append field, will stop the running one.
                            repeatTimes=0                 #this will stop the animation at the next iteration
                    except IndexError as e:
                        pass

                    frame=frames[frameIdx%animation_len]["leds"]
                    NEOPIXEL_LEN=len(frame)
                    led_ctl_string=""
                    for ledIdx in range(0,NEOPIXEL_LEN):
                        pixel=frame[ledIdx]
                        r=int(pixel['r']*DIM_PERCENT/100)
                        g=int(pixel['g']*DIM_PERCENT/100)
                        b=int(pixel['b']*DIM_PERCENT/100)
                        led_ctl_string=led_ctl_string+str(ledIdx)+"\n"+str(r)+"\n"+str(g)+"\n"+str(b)+"\n"

                    ENABLE_DOUBLE_BUFFERING=False #for low speed animation leave this to false
                    if ENABLE_DOUBLE_BUFFERING:
                        led_process.sendline(led_ctl_string+"d\nb")
                    else:
                        led_process.sendline(led_ctl_string+"d")
                    led_process.expect("Done!")
                    #  print("neopixel_set_rgb response: "+led_process.before)
                    frameIdx=frameIdx+1

                    ex_time=time.time()-init_time
                    sleep_interval=frame_interval-ex_time
                    if sleep_interval<0.001: #sleep at least this time to avoid taking 100% of cpu
                        print("Warning: the time to draw the frame ("+str(ex_time*1000)+"ms)  is longer than the requested interframe interval!")
                        sleep_interval=0.001
                    #print("frameidx: "+str(frameIdx)+" took: "+str(ex_time*1000)+"ms")
                    time.sleep(sleep_interval)
                    init_time=time.time()
                running_animation=None
            #except Exception as e:
            #    print("Failed. "+ str(e))
            #    time.sleep(1)
            #    pass

            except IndexError as e:
                pass

class USER_INPUT_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def run(self):
        f=0
        while self.__loop:
            try:
                input_str = input()

                f=open(input_str, "r")
                message=f.read()
                jmessage=json.loads(message)
                message_type=jmessage['messageType']
                if message_type=="animation":
                    message_content=jmessage['content']
                    neopixel_queue.append(message_content)
                    print("New animation loaded!")
                    print("Enter the animation filename:")
            except Exception as e:
                print("Read failed. "+ str(e))
                pass

    def stop(self):
        self.__loop=False

neopixel_queue=deque()

neopixel_thread=NEOPIXEL_THREAD(1,"neopixel thread")
neopixel_thread.setDaemon(True)
neopixel_thread.start()

while not neopixel_thread.initialized:
    time.sleep(0.01)

user_input_thread=USER_INPUT_THREAD(2,"user input thread")
user_input_thread.setDaemon(True)
user_input_thread.start()

print("All animations will be dimmed at "+str(DIM_PERCENT)+"%. To change it, edit the DIM_PERCENT variable in this file.")

print("Enter the animation filename:")

while True:
    time.sleep(1)


