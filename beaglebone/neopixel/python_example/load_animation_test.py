import pexpect
import time
import sys
import json
from collections import deque

DEFAULT_ANIMATION_FILENAME="message_dump.txt"

if len(sys.argv)>1:
    animation_filename=sys.argv[1]
else:
    animation_filename=DEFAULT_ANIMATION_FILENAME

positive_neopixel_response="waiting for response from pru0... OK\r\n"
def initialize_neopixel():
    m_led_process=pexpect.spawnu("./../bin/neopixel_set_rgb 12 ./.../bin/ws281x.bin",echo=False)
    m_led_process.expect(positive_neopixel_response,timeout=10)
    print(m_led_process.before+positive_neopixel_response[:-2])
    init_ok=True
    return m_led_process

#NOTE: the neopixel program always fail when relaunched. However it always work if at least two attempts are made.
# The next section tries at most two times

try:
    led_process=initialize_neopixel()
    print("initialized 1!")
except pexpect.exceptions.EOF as e:
    print("Exception during neopixel initialization. It is typical to get this message at least once. The programm will retry...")
    time.sleep(0.1)
    led_process=initialize_neopixel()
    print("initialized 2!")



f=open(animation_filename, "r")
message=f.read()
jmessage=json.loads(message)

neopixel_queue=deque()
neopixel_queue.append(jmessage)


running_animation=neopixel_queue.popleft()
try:
    frames=jmessage['frames']
    frame_interval=jmessage['frameInterval']
    #print(str(frame_interval*1000))
    append=jmessage['append']
    repeatTimes=jmessage['repeat']
    frameIdx=0
    init_time=time.time()
    animation_len=len(frames)
    print("The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps. It will last: "+str(animation_len*frame_interval)+"s")
    while frameIdx<(animation_len*repeatTimes) or repeatTimes==-1:
        frame=frames[frameIdx%animation_len]["leds"]
        NEOPIXEL_LEN=len(frame)
        led_ctl_string=""
        for ledIdx in range(0,NEOPIXEL_LEN):
            pixel=frame[ledIdx]
            r=pixel['r']
            g=pixel['g']
            b=pixel['b']
            led_ctl_string=led_ctl_string+str(ledIdx)+"\n"+str(r)+"\n"+str(g)+"\n"+str(b)+"\n"
        
        ENABLE_DOUBLE_BUFFERING=False #for low speed animation leave this to false
        if ENABLE_DOUBLE_BUFFERING:
            led_process.sendline(led_ctl_string+"d\nb")
        else:
             led_process.sendline(led_ctl_string+"d")
        led_process.expect("Done!")
        #  print("neopixel_set_rgb response: "+led_process.before)
        frameIdx=frameIdx+1
         
        ex_time=time.time()-init_time
        sleep_interval=frame_interval-ex_time
        if sleep_interval<0:
            print("Warning: the time to draw the frame ("+str(ex_time*1000)+"ms)  is longer than the requested interframe interval!")
            sleep_interval=0
        print("frameidx: "+str(frameIdx)+" took: "+str(ex_time*1000)+"ms")
        time.sleep(sleep_interval)
        init_time=time.time()
except Exception as e:
    print("Failed. "+ str(e))
    time.sleep(1)
    pass

print("the end!")
