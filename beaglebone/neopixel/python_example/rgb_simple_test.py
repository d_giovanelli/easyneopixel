import pexpect
import time

positive_neopixel_response="waiting for response from pru0... OK\r\n"
def initialize_neopixel():
    m_led_process=pexpect.spawnu("./../bin/neopixel_set_rgb 12 ./../bin/ws281x.bin",echo=False)
    m_led_process.expect(positive_neopixel_response,timeout=10)
    print(m_led_process.before+positive_neopixel_response[:-2])
    init_ok=True
    return m_led_process

#NOTE: the neopixel program always fail when relaunched. However it always work if at least two attempts are made.
# The next section tries at most two times

try:
    led_process=initialize_neopixel()
    print("initialized 1!")
except pexpect.exceptions.EOF as e:
    print("Exception during neopixel initialization. It is typical to get this message at least once. The programm will retry...")
    time.sleep(0.1)
    led_process=initialize_neopixel()
    print("initialized 2!")

stat=0
while True:
    led_ctl_string=""
    try:
        r=((stat>>1)&1)*255
        g=((stat>>2)&1)*255
        b=((stat>>3)&1)*255
        p=stat%12
        led_ctl_string=led_ctl_string+str(p)+"\n"+str(r)+"\n"+str(g)+"\n"+str(b)+"\n"

        #led_process.sendline(str(p)+"\n"+str(r)+"\n"+str(g)+"\n"+str(b))
        #led_process.expect('\n')
        if p == 0:
            print("drawing! "+str(stat/12))
            ENABLE_DOUBLE_BUFFERING=False
            if ENABLE_DOUBLE_BUFFERING:
                led_process.sendline(led_ctl_string+"d\nb")
            else:
                led_process.sendline(led_ctl_string+"d")

            led_process.expect('Done!')
        
        stat=stat+1
        #time.sleep(0.01)
    except Exception as e:
        print("Failed. "+ str(e))
        time.sleep(1)
        pass
