Neopixel simple control from comand line

This folder contains the binaries to control the Neopixel led strips from the comand line using a simplified interface.

The used libraries are taked from the PixelBone repository (https://github.com/toroidal-code/PixelBone/), only two compiled binaries are considered now:
bin/neopixel_set_rgb : real program that controls the Neopixels
bin/ws281x.bin : binary that is loaded into the pru, it needs to be passed to bin/neopixel_set_rgb as argument

A simple python test for the neopixels (12 leds) is also present, it simply uses pexpect to execute the program in a dedicated process. To test the neopixels, connect GND->DGND(P9_1), VDD->VDD_3V3(P9_3) and DATA_IN->GPIO_20(P9_41) and execute the python (sudo is required to use prus):
	sudo python3 test.py