Simple Neopixel control

Super basic interface to control the Neopixel led strips.
To use it:
	sudo ./neopixel_set_rgb 12 ./ws281x.bin

Where '12' is the length of the strip and ws281x.bin is the binary that will be loaded into the PRU
Once launched the program perform a blocking read to stdint (scanf) waiting for n-ple indicating the LED index and the RGB color, then to turn on the LED with index 0, with R=10, G=200, B=43:
	0 10 200 43
to turn on the last LED (index 11) with R=0, G=0, B=90
	11 0 0 90

To exit the program send any RGB color to an LED index higher than the maximum allowed. In the previous experiment
	12 0 0 0
	
Attention: for some reason the GPIO sometimes is not accessible and launching neopixel_set_rgb ends up with " Unable to open? No such file or directory", I don't know why this happens since the GPIO should not be unexported. In any case to workaround this, just call the program again (remember to use sudo).
