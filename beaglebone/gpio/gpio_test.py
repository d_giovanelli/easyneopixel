import time
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
import pexpect
import threading

IN_GPIO="P8_12"
OUT_GPIO="P8_14"
PWM_GPIO="P8_13"

def gpio_change_handler(gpio):
    if gpio==IN_GPIO:
        input_val=GPIO.input(IN_GPIO)
        print(IN_GPIO+" changed to -> "+str(input_val)+". Copying to "+OUT_GPIO)
        GPIO.output(OUT_GPIO,input_val)
    else:
        print("Event not the IN_GPIO: "+IN_GPIO+" but on: "+gpio)

def GPIO_setup():
    pIn=pexpect.spawn("config-pin "+IN_GPIO+" gpio")
    pIn.expect(pexpect.EOF)
    pOut=pexpect.spawn("config-pin "+OUT_GPIO+" gpio")
    pOut.expect(pexpect.EOF)

    GPIO.setup(IN_GPIO, GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(IN_GPIO, GPIO.BOTH, gpio_change_handler, 5)
    print("Input GPIO "+IN_GPIO+" ready!")
    GPIO.setup(OUT_GPIO, GPIO.OUT, initial=0)
    print("Output GPIO "+OUT_GPIO+" ready!")
    PWM.start(PWM_GPIO,0)
    print("PWM GPIO "+PWM_GPIO+" ready!")

def startPWM(gpio,dc,freq,timeout=0):
    print("startPWM. gpio: "+gpio+" dc: "+str(dc)+" freq: "+str(freq)+" timeout:"+str(timeout))
    if timeout==0:
        PWM.start(gpio,dc,freq)
    else:
        PWM.start(gpio,dc,freq)
        #todo add timeout
        threading.Timer(timeout,PWMTimeoutHandler,[gpio]).start()

def stopPWM(gpio):
    print("stopPWM. gpio: "+gpio)
    PWM.stop(gpio)

def PWMTimeoutHandler(gpio):
    print("Timeout handler for PWM on "+gpio)
    stopPWM(gpio)

class USER_INPUT_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def run(self):
        f=0
        while self.__loop:
            try:
                input_str = input()

                if input_str=="p":
                    f=f+2000
                    if f<=10000:
                        startPWM(PWM_GPIO,50,f,timeout=1)
                        print("Turning PWM to "+str(f/1000)+" kHz for 1s")
                    else:
                        stopPWM(PWM_GPIO)
                        print("Turning PWM off")
                        f=0
                else:
                    print(input_str+" is an invalid input!")
            except Exception as e:
                print("Read failed. "+ str(e))
                pass


GPIO_setup()

user_input_thread=USER_INPUT_THREAD(1,"user input thread")
user_input_thread.setDaemon(True)
user_input_thread.start()

while True:
    
    time.sleep(0.001)


