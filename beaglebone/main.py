import pexpect
import time
import threading
import sys
from collections import OrderedDict
from collections import deque
import json
import paho.mqtt.client as mqtt
import traceback
import base64
import os
import uuid

#MQTT ROOT TOPICS
TELEMETRY_TOPIC="telemetry"
RPC_REQUEST_TOPIC="rpc/request"
RPC_RESPONSE_TOPIC="rpc/response"

#MQTT DEVICE TOPIC
DEVICE_TOPIC="" #an ID to use. Set at boot

#MQTT SERVICES TOPIC
SOUND_SERVICE_TOPIC="sound"
NFC_SERVICE_TOPIC="nfc"
UI_SERVICE_TOPIC="ui"

#MQTT CHARACTERISTIC TOPIC
NEOPIXEL_CHARACTERISTIC_TOPIC="neopixel"

#MQTT CONFIGURATION
MQTT_BROKER_DEF="i3app-rabbit.fbk.eu" #"iot.smartcommunitylab.it"
MQTT_PORT_DEF=1883
DEVICE_TOKEN_DEF="test_vhost:i3usr"  #"MnAZcwSpqG3XjGO4RxJy" #FIRST 4 READERS PROTOTYPE: "0alJMWpXlOcY8bZuWxvK", SECOND 4 READERS PROTOTYPE: "B43vCbweCWihtUCn6vln", BLUETOOTH PROTOTYPE: MnAZcwSpqG3XjGO4RxJy
DEVICE_PASSWORD_DEF="aaa" #"None"

#ERRORS
SUCCESS=0                   #everithing ok
UNKNOWN_SERVICE=1           #the characteristic is unknwonw
UNKNOWN_CHARACTERISTIC=2    #the characteristic is unknwonw
UNKNOWN_METHOD=3            #the parameter is unknwonw
VALUE_ERROR=4               #the value is not valid (i.e. the interval must be within 1 and 10 second, but 0.1 was requested)
UNKNOWN_ERROR=5             #the value is not valid (i.e. the interval must be within 1 and 10 second, but 0.1 was requested)
FILE_ERROR=6                #generic error (exception) during file handling
FILE_NOT_FOUND_ERROR=7      #requested file does not exist
ADDRESS_ERROR=8             #the message is not addressed to me

mqtt_broker=MQTT_BROKER_DEF
mqtt_port=MQTT_PORT_DEF
device_token=DEVICE_TOKEN_DEF
device_password=DEVICE_PASSWORD_DEF #shall this be added to command line arguments?

DEVICE_READABLE_NAME="test beaglebone"

################################################  MQTT CALLBACKS  #############################################

def on_publish_cb(client, data, result):
    print("[BEAGLEBONE - MQTT] on_publish. data: "+ str(data) +", result: "+str(result))
    pass

def on_disconnect_cb(client, userdata, result):
    global mqtt_connection_active
    print("[BEAGLEBONE - MQTT] on_disconnect_cb.")
    mqtt_connection_active=False
    pass

# The callback for when the client receives a CONNACK response from the server.
def on_connect_cb(client, userdata, flags, rc):
    global mqtt_connection_active

    if rc==0:
        mqtt_connection_active=True #TODO: This doesn't change value, maybe threads problem
    else:
        mqtt_connection_active=False

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.  

    subscribe_topic=RPC_REQUEST_TOPIC+'/'+DEVICE_TOPIC+"/#"
    print("[BEAGLEBONE - MQTT] on connect result code "+str(rc)+". Subscribing to topic: "+subscribe_topic)
    client.subscribe(subscribe_topic)

def on_message_cb(client, userdata, msg):
    try:
        dec_message=msg.payload.decode("utf-8")
        ret = message_handler(dec_message,msg.topic)
        
        if ret==ADDRESS_ERROR or ret==UNKNOWN_SERVICE or ret==UNKNOWN_CHARACTERISTIC or ret==UNKNOWN_METHOD:
            multi_nfc_message_hander(dec_message,msg.topic)

    except Exception as e:
        print("[BEAGLEBONE - MQTT] Exception in on_message!: "+ str(e))
        traceback.print_exc()

################################################  MQTT HELPERS  #############################################

def connect_mqtt():

    print("[BEAGLEBONE - MQTT] Connecting...")
    m_client=mqtt.Client()
    m_client.on_publish=on_publish_cb
    m_client.on_connect=on_connect_cb
    m_client.on_message=on_message_cb
    m_client.on_disconnect=on_disconnect_cb

    m_client.username_pw_set(device_token, device_password)
    m_client.connect(mqtt_broker, mqtt_port)
    return m_client

def publish_mqtt(mqtt_client,data_to_publish,topic):
    global mqtt_connection_active

    qos=1
    try:
        msg_uuid=str(uuid.uuid4())
        data_to_publish['messageID']=msg_uuid

        print("[BEAGLEBONE - MQTT] Publishing this data: "+json.dumps(data_to_publish)+" on topic: "+topic)
        ret=mqtt_client.publish(topic,json.dumps(data_to_publish),qos)
        print("[BEAGLEBONE - MQTT] Publish return code: "+str(ret.rc))
    except Exception as e:
        print("[BEAGLEBONE - MQTT] Exception in publish_mqtt!: "+ str(e))

def send_rpc_response(append_topic, method, responseID, return_value):
    topic=RPC_RESPONSE_TOPIC+'/'+DEVICE_TOPIC+'/'+append_topic
    response_msg={'method':method , 'inReplyTo':responseID, 'params':{'return':return_value}, 'path':append_topic}
    publish_mqtt(mqtt_client,response_msg,topic)

def send_telemetry(sensorClass,sensorType,reading):
    global mqtt_client
    global DEVICE_ID
    global DEVICE_TOPIC

    topic=TELEMETRY_TOPIC+'/'+DEVICE_TOPIC
    if sensorClass != None:
        topic=topic+'/'+sensorClass
    else:
        sensorClass=""

    if sensorType != None:
        topic=topic+'/'+sensorType
    else:
        sensorType=""

    msg={"sensorClass":sensorClass, "sensorType":sensorType, "reading":reading, "sourceName":DEVICE_READABLE_NAME, "sourceUID":DEVICE_ID}
    
    publish_mqtt(mqtt_client,msg,topic)

def message_handler(msg,topic):
    try:
        jobj=json.loads(msg)
        
        print("[BEAGLEBONE - MQTT] message received with topic: "+topic) #+" msg: "+str(jobj))


        topic_parts=topic.split('/')
        #TODO Filter based on THINGY topic and reject other requests
        if (topic_parts[0]+'/'+topic_parts[1]+'/'+topic_parts[2]==RPC_REQUEST_TOPIC+'/'+DEVICE_TOPIC): #the message is for me
            target_service=None
            target_characteristic=None

            responseTopic=""

            if len(topic_parts)>=4:
                target_service=topic_parts[3]
                responseTopic=target_service

            if len(topic_parts)>=5:
                target_characteristic=topic_parts[4]
                responseTopic=responseTopic+'/'+target_characteristic


            messageID=jobj["messageID"]
            request_method=jobj["method"]
            request_params=None
            try:
                request_params=jobj["params"]
            except KeyError as e:
                print("[BEAGLEBONE - MQTT] WARNING: No params found in the message.")
                pass

            ret = processHumanReadConfigRequest(target_service,target_characteristic,request_method,request_params,messageID)
            
            send_rpc_response(responseTopic,request_method,messageID,ret)

            return ret
        else:
            print("[BEAGLEBONE - MQTT] The message is not for me..."+topic_parts[0]+'/'+topic_parts[1]+'/'+topic_parts[2]+" != " +RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC)
            #for p in topic_parts:
            #    print("[MQTT] "+p)
            return ADDRESS_ERROR
            pass

    except Exception as e:
        #with print_lock:
        print("[BEAGLEBONE - MQTT] Exception in message_handler!: "+ str(e))
        traceback.print_exc()
        return UNKNOWN_ERROR

################################################  MQTT RPC HANDLERS  #############################################

def processHumanReadConfigRequest(target_service,target_characteristic,request_method,request_params,messageID):

    if target_service==SOUND_SERVICE_TOPIC:
        print("[BEAGLEBONE - RPC] processHumanReadConfigRequest - target_service: "+target_service+" target_characteristic: None request_method: "+request_method)
        return processSoundConfigReq(target_characteristic,request_method,request_params,messageID)
    elif target_service==NFC_SERVICE_TOPIC:
        print("[BEAGLEBONE - RPC] processHumanReadConfigRequest - target_service: "+target_service+" target_characteristic: "+target_characteristic+" request_method: "+request_method)
        return processNfcConfigReq(target_characteristic,request_method,request_params,messageID)

    elif target_service==UI_SERVICE_TOPIC:
        print("[BEAGLEBONE - RPC] processHumanReadConfigRequest - target_service: "+target_service+" target_characteristic: "+target_characteristic+" request_method: "+request_method)
        return processUiConfigReq(target_characteristic,request_method,request_params,messageID)

    else:
        print("[BEAGLEBONE - RPC] Config of service: "+str(target_service)+" not available!")
        return UNKNOWN_SERVICE

def processSoundConfigReq(target_characteristic,request_method,request_params,messageID):
    if target_characteristic != None: #for now audio service doesn't have characteristics
        return UNKNOWN_CHARACTERISTIC

    if request_method == "load-file":
        print("[BEAGLEBONE - RPC] load audio method!!!")
        return save_file(request_params)
    elif request_method == "play-audio":
        print("[BEAGLEBONE - RPC] play audio method!!! "+str(request_params))
        request_params['messageID']=messageID #in this way messageID is encapsulated into request params and it is easier to pass the value to the audio thread
        return play_audio_file_request(request_params)
    else:
        return UNKNOWN_METHOD

def processNfcConfigReq(target_characteristic,request_method,request_params,messageID):
    #no configuration for nfc for now. They only send telemetry with NFCID1 data and buttons
    #in case something is added (such as rgb led of the first prototype), before send to subprocess do the filtering. target_characteristic will be the so called reader address
    # p is the target process:
    #    p.sendline(request_params)
    return UNKNOWN_METHOD

def processUiConfigReq(target_characteristic,request_method,request_params,messageID):
    if target_characteristic==NEOPIXEL_CHARACTERISTIC_TOPIC:
        if request_method == 'play-animation':
            request_params['messageID']=messageID #copy the id in the params field to make it easy to return the messageID value once the animation is finished
            return play_neopixel_request(request_params)
        else:
            return UNKNOWN_METHOD
    else:
        return UNKNOWN_CHARACTERISTIC

################################################  USER INPUT THREAD  #############################################

class USER_INPUT_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def run(self):
        while self.__loop:
            try:
                input_str = input()
                nfc_thread.send_input(input_str)
            except Exception as e:
                print("[BEAGLEBONE - INPUT] Read failed. "+ str(e))
                pass

################################################  NFC #############################################

#edit (change, add or remove entries) the next list to change the readers ports and device_addresses
# the list contains: the uart name, the reader address and gpios used by the reader in this order: [uart_name,reader_address,buttonGPIO,ledRedGPIO,ledGreenGPIO,ledBlueGPIO]
READERS_DEF=list([
    ["/dev/ttyS1","115200","object_antenna0","P8_45","P8_43","P8_41","P8_39"],
    #["/dev/ttyS2","115200","label_antenna1","P8_36","P8_34","P8_32","P8_30"],
    #["/dev/ttyS4","115200","label_antenna2","P8_35","P8_33","P8_31","P8_29"],
    #["/dev/ttyS5","115200","label_antenna3","P8_46","P8_44","P8_42","P8_40"],
    #["/dev/rfcomm0","115200","bluetooth_antenna0","NONE","NONE","NONE","NONE"],
])

READER_SCRIPT_PATH_DEF ="PN532/python_PN532/main.py"
reader_script_path=READER_SCRIPT_PATH_DEF

class NFC_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID=threadID
        self.name = name
        self.__loop=True
        self.__processes=list()

    def __initialize(self):
        cmd_list=[]
        for reader in READERS_DEF:
            cmd="python3 "+reader_script_path+" "+reader[0]+" "+reader[1]+" "+reader[2]+" "+reader[3]+" "+reader[4]+" "+reader[5]+" "+reader[6]
            cmd_list.append(cmd)

        print("[BEAGLEBONE - NFC] Will call these commands:")
        for c in cmd_list:
            print("    "+c)

        for c in cmd_list:
            self.__processes.append(pexpect.spawnu(c,echo=False))

        print("[BEAGLEBONE - NFC] All started!")

    def send_input(self,input_str):
        for p in self.__processes:
            p.sendline(input_str)

    def run(self):

        self.__initialize()

        while self.__loop:
            eof_ex_shown=[False]*len(self.__processes)

            for i in range(len(self.__processes)):
                try:
                    self.__processes[i].expect('\n',timeout=0)
                    proc_data=self.__processes[i].before
                    try:
                        json_data=json.loads(proc_data)
                        #TODO: use send_telemetry
                        #publish_mqtt(json.loads(proc_data))
                        characteristic_topic=json_data["readerAddress"]
                        send_telemetry(NFC_SERVICE_TOPIC, characteristic_topic,json_data)
                    except json.JSONDecodeError:
                        print("[BEAGLEBONE - NFC - READER "+str(i)+"] : " + proc_data)

                except pexpect.exceptions.TIMEOUT:
                    pass
                except pexpect.exceptions.EOF:
                    if not eof_ex_shown[i]:
                        print("[BEAGLEBONE - NFC - READER "+str(i)+"] : error during reader startup. That reader likelly won't work!")
                        eof_ex_shown[i]=True
                    pass
            time.sleep(0.01)


################################################  NEOPIXEL #############################################

DIM_PERCENT=10

positive_neopixel_response="waiting for response from pru0... OK\r\n"
def initialize_neopixel():
    m_led_process=pexpect.spawnu("neopixel/bin/neopixel_set_rgb 12 neopixel/bin/ws281x.bin",echo=False)
    m_led_process.expect(positive_neopixel_response,timeout=10)
    #print(m_led_process.before+positive_neopixel_response[:-2])
    init_ok=True
    return m_led_process

def play_neopixel_request(params):
    global neopixel_queue
    neopixel_queue.append(params)
    return SUCCESS #cannot return anything meaninful here. Audio process happen on a separated thread. The thread will return some meaninful thing once played

class NEOPIXEL_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True
        self.initialized=False

    def run(self):
        global neopixel_queue
        f=0
        #NOTE: the neopixel program always fail when relaunched. However it always work if at least two attempts are made.
        # The next section tries at most two times
        try:
            led_process=initialize_neopixel()
            self.initialized=True
            print("[BEAGLEBONE - NEOPIXEL] Neopixel initialized at first trial.")
        except pexpect.exceptions.EOF as e:
            #print("Exception during neopixel initialization. It is typical to get this message at least once. The programm will retry...")
            time.sleep(0.1)
            led_process=initialize_neopixel()
            self.initialized=True
            print("[BEAGLEBONE - NEOPIXEL] Neopixel initialized at second trial.")

        while self.__loop:
            try:
                running_animation_request=neopixel_queue.popleft()
                print("[BEAGLEBONE - NEOPIXEL] new animation pop from the queue. There are "+str(len(neopixel_queue))+" remaining animation in the queue")
                frames=running_animation_request['frames']
                frame_interval=running_animation_request['frameInterval']
                #print(str(frame_interval*1000))
                append=running_animation_request['append']
                repeatTimes=running_animation_request['repeat']
                frameIdx=0
                init_time=time.time()
                animation_len=len(frames)
                animation_uuid=running_animation_request["messageID"]
                interrupted_animation_flag=False
                if repeatTimes>0:
                    print("[BEAGLEBONE - NEOPIXEL] The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps and it will be repeated "+str(repeatTimes)+" times. In total it will last: "+str(animation_len*frame_interval*repeatTimes)+"s")
                else:
                    print("[BEAGLEBONE - NEOPIXEL] The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps. It will be repeated till a new animation is received (repeat=-1)")
               
                while frameIdx<(animation_len*repeatTimes) or repeatTimes==-1:
                    try: #TODO: this check should be done at the multireader_message_handler, otherwise if two appends are done in a short time it might loose an append=False condition
                        #if animation_that_overwrites!=None:
                        new_animation=neopixel_queue[-1] #do not pop the last animation, just check for the append field.
                        if not new_animation['append'] or repeatTimes==-1: #if the new animation has to overwrite the running one then stop it, the new one will be pop$
                            repeatTimes=0                 #this will stop the animation at the next iteration
                            neopixel_queue.clear()
                            neopixel_queue.append(new_animation) #leave only the last animation in the queue
                            interrupted_animation_flag=True
                    except IndexError as e:
                        pass

                    frame=frames[frameIdx%animation_len]["leds"]
                    NEOPIXEL_LEN=len(frame)
                    led_ctl_string=""
                    for ledIdx in range(0,NEOPIXEL_LEN):
                        pixel=frame[ledIdx]
                        r=int(pixel['r']*DIM_PERCENT/100)
                        g=int(pixel['g']*DIM_PERCENT/100)
                        b=int(pixel['b']*DIM_PERCENT/100)
                        led_ctl_string=led_ctl_string+str(ledIdx)+"\n"+str(r)+"\n"+str(g)+"\n"+str(b)+"\n"

                    ENABLE_DOUBLE_BUFFERING=False #for low speed animation leave this to false
                    if ENABLE_DOUBLE_BUFFERING:
                        led_process.sendline(led_ctl_string+"d\nb")
                    else:
                        led_process.sendline(led_ctl_string+"d")
                    led_process.expect("Done!")
                    #  print("neopixel_set_rgb response: "+led_process.before)
                    frameIdx=frameIdx+1

                    ex_time=time.time()-init_time
                    sleep_interval=frame_interval-ex_time
                    if sleep_interval<0.001: #sleep at least this time to avoid taking 100% of cpu
                        print("[BEAGLEBONE - NEOPIXEL] Warning: the time to draw the frame ("+str(ex_time*1000)+"ms)  is longer than the requested interframe interval!")
                        sleep_interval=0.001
                    #print("frameidx: "+str(frameIdx)+" took: "+str(ex_time*1000)+"ms")
                    time.sleep(sleep_interval)
                    init_time=time.time()

                if interrupted_animation_flag:
                    ret="interrupted"
                else:
                    ret="executed"

                request_method="neopixelAnimation"
                responseTopic=UI_SERVICE_TOPIC+'/'+NEOPIXEL_CHARACTERISTIC_TOPIC
                send_rpc_response(responseTopic,request_method,running_animation_request['messageID'],ret)

                running_animation_request=None
            except IndexError as e:
                time.sleep(0.1)
                pass

################################################  SOUND  #############################################

LOCAL_RESOURCES_RELATIVE_PATH="../resources/"

def play_audio(file_path):

    file_exists=False
    print("[BEAGLEBONE - SOUND] Playing audio file: "+file_path)
    try:
        with open(file_path, "r") as f:
            file_exists=True
    except FileNotFoundError:
        print('[BEAGLEBONE - SOUND] Requested audio file does not exist')

    if file_exists:
        return pexpect.spawnu("mpg123 " + file_path,echo=False)
    else:
        return None #FILE_NOT_FOUND_ERROR

def play_audio_file_request(params):
    global sound_play_queue
    sound_play_queue.append(params)
    print('[BEAGLEBONE - SOUND] Play request added to the queue. Queue size: '+str(len(sound_play_queue)))

    return SUCCESS #cannot return anything meaninful here. Audio process happen on a separated thread. The thread will return some meaninful thing once played

def save_file(file_data):
    try:
        fileName=file_data['filename']
        filePath=LOCAL_RESOURCES_RELATIVE_PATH+fileName
        file_raw=base64.b64decode(file_data['rawData'],validate=True)

        with open(filePath, "wb") as f:
            f.write(file_raw)

        return SUCCESS

    except Exception as e:
        print("[BEAGLEBONE - SOUND] Error occured during file save.")
        traceback.print_exc()
        return FILE_ERROR

class SOUND_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True
        self.initialized=False

    def run(self):
        global sound_play_queue
        while self.__loop:
            try:
                running_sound_request=sound_play_queue.popleft()
                print("[BEAGLEBONE - SOUND] new audio pop from the queue. There are "+str(len(sound_play_queue))+" remaining audio in the queue")

                #append=running_sound_request['append']
                repeatTimes=running_sound_request['repeat']
                interrupted_sound_flag=False
                if repeatTimes>0:
                    print("[BEAGLEBONE - SOUND] The sound will be repeated "+str(repeatTimes)+" times.")
                else:
                    print("[BEAGLEBONE - SOUND] The sound will be repeated till a new one is received (repeat=-1)")
                
                fileName=running_sound_request['filename']
                filePath=LOCAL_RESOURCES_RELATIVE_PATH+fileName
                audio_played_times=0
                play_invoked=False

                while (audio_played_times<repeatTimes or repeatTimes==-1): # and audio_player_process!=None:
                    try: #TODO: this check should be done at the multireader_message_handler, otherwise if two appends are done in a short time it might loose an append=False condition
                        new_sound=sound_play_queue[-1] #do not pop the last sound, just check for the append field.
                        if not new_sound['append'] or repeatTimes==-1: #if the new sound has to overwrite the running one then stop it, the new one will be pop$
                            repeatTimes=0                 #this will stop the sound at the next loop
                            sound_play_queue.clear()
                            sound_play_queue.append(new_sound) #leave only the last sound in the queue
                            interrupted_sound_flag=True
                            print("[BEAGLEBONE - SOUND] The last sound received requested to interrupt the one playing.")
                    except IndexError as e:
                        pass
                    try:
                        if not play_invoked:
                            audio_player_process=play_audio(filePath)
                            if audio_player_process!=None:
                                play_invoked=True
                            else:
                                ret="error"
                                request_method="play-audio"
                                responseTopic=SOUND_SERVICE_TOPIC
                                send_rpc_response(responseTopic,request_method,running_sound_request['messageID'],ret)
                                break
                        audio_player_process.expect(pexpect.EOF,timeout=0.05)
                        play_invoked=False
                        audio_played_times+=1
                        continue    #if the audio has finished, restart the loop immediately
                    except pexpect.exceptions.EOF:
                        play_invoked=False
                        audio_played_times+=1 #I don't know if it can rise and EOF, in case it does just count it as played audio.
                        continue    #if the audio has finished, restart the loop immediately
                    except pexpect.exceptions.TIMEOUT:
                        #during normal audio play the execution will go here, audio_played_times is incremented only when EOF is found, i.e. when the sound has been executed and the player exits
                        pass

                    if interrupted_sound_flag:
                        audio_player_process.kill(0)

                if interrupted_sound_flag:
                    ret="interrupted"
                else:
                    ret="executed"

                request_method="play-audio"
                responseTopic=SOUND_SERVICE_TOPIC
                send_rpc_response(responseTopic,request_method,running_sound_request['messageID'],ret)

                running_sound_request=None
            except IndexError as e:
                time.sleep(0.1)
                pass


################################################  APPLICATION  #############################################

DEVICE_ID="1234"
DEVICE_TOPIC=DEVICE_ID+"-beaglebone"

mqtt_client=None
while mqtt_client==None:
    try:
        mqtt_client=connect_mqtt()
    except Exception as e:
        print("[BEAGLEBONE - APPLICATION] Exception during mqtt connect. " + str(e))
        time.sleep(1)

try:
    mqtt_client.loop_start()
except Exception as e:
    print("[BEAGLEBONE - APPLICATION] Exception during mqtt loop start. " + str(e))


nfc_thread=NFC_THREAD(1,"nfc thread")
nfc_thread.setDaemon(True)
nfc_thread.start()

neopixel_queue=deque()
neopixel_thread=NEOPIXEL_THREAD(2,"neopixel thread")
neopixel_thread.setDaemon(True)
neopixel_thread.start()

sound_play_queue=deque()
sound_thread=SOUND_THREAD(3,"sound thread")
sound_thread.setDaemon(True)
sound_thread.start()

user_input_thread=USER_INPUT_THREAD(4,"user input thread")
user_input_thread.setDaemon(True)
user_input_thread.start()

time.sleep(1) #give some time to all the threads to start

message={"ready":True}
send_telemetry(None,None,message) 

while (1):
    time.sleep(1)

