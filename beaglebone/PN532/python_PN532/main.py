import serial
import sys
import time
import threading
import queue
from enum import IntEnum, unique
import binascii
import json
from collections import deque
import Adafruit_BBIO.GPIO as GPIO
import pexpect
import traceback
import signal

#Default values for comand line arguments
SERIAL_PORT_DEF = "/dev/ttyS0" #"/dev/ttyS0"
RUNTIME_BAUDRATE_DEF =  115200
READER_ADDRESS_DEF = "bluetooth0"


#settings
AUTOSTART=True
PN532_ack_timeout_value=0.040    #nominnally this should be 15ms, however experimentally it can be higher (maybe because of slow python execution). 46ms is the experimental 99 percentile, 32ms is the 90 percentile (avg 20ms) (with 5 passive targets)
PN532_response_timeout_value=0.086 #0.12    #this is a soft timeout, for instance if InListPassiveTargets is called, and not target is present no response is generated till the target is put on the reader.  121ms is the experimental 99 percentile, 86ms is the 90 percentile (avg 70ms) (with 5 passive targets)
TARGET_TIMEOUT_VALUE_DEF=None#10  #set to None to use the automatic timeout

TARGET_TIMEOUT_Q=0.3    #unit is [s] (for rfcomm interface this might change, see after the command line arguments parsing)
TARGET_TIMEOUT_M=0.1    #unit is [s/#numberOfTargets] (for rfcomm interface this might change, see after the command line arguments parsing)

#constants (do not edit)
RESET_BAUDRATE = 115200
START_OF_PACKET=[0x00, 0xFF]
POSTABLE=[0x00]

#command line arguments parsing
if len(sys.argv)>1:
    serial_port=sys.argv[1]
else:
    serial_port=SERIAL_PORT_DEF
if len(sys.argv)>2:
    try:
        arg_baudrate=int(sys.argv[2])
        if arg_baudrate!=0:
            runtime_baudrate=arg_baudrate
        else:
            runtime_baudrate=RUNTIME_BAUDRATE_DEF
    except Exception as e:
        print("Error occured during uart baudrate parsing. Uart baudrate is the second argument, set it to 0 to use default value: "+str(RUNTIME_BAUDRATE))
        runtime_baudrate=RUNTIME_BAUDRATE_DEF
        pass
else:
    runtime_baudrate=RUNTIME_BAUDRATE_DEF

if len(sys.argv)>3:
    reader_address=sys.argv[3]
else:
    reader_address=READER_ADDRESS_DEF
if len(sys.argv)>4:
    button_gpio=sys.argv[4]
else:
    button_gpio="NONE"
if len(sys.argv)>5:
    led_red_gpio=sys.argv[5]
else:
    led_red_gpio="NONE"
if len(sys.argv)>6:
    led_green_gpio=sys.argv[6]
else:
    led_green_gpio="NONE"
if len(sys.argv)>7:
    led_blue_gpio=sys.argv[7]
else:
    led_blue_gpio="NONE"


use_PN532s_gpios=(serial_port.count("rfcomm")>0)
if (serial_port.count("rfcomm")>0) and (button_gpio!="NONE" or led_red_gpio!="NONE" or led_green_gpio!="NONE" or led_blue_gpio!="NONE"):
    print("[BOOT] WARNING: an RFCOMM is selected (bluetooth reader), however gpios in command line arguments are not NONE.")
    print("[BOOT] WARNING: the gpios configuration will be discarded, the PN532 gpios will be used for button and LEDs.")
    use_PN532s_gpios=True
    button_gpio="NONE"
    led_red_gpio="NONE"
    led_green_gpio="NONE"
    led_blue_gpio="NONE"

if button_gpio=="NONE" and led_red_gpio=="NONE" and led_green_gpio=="NONE" and led_blue_gpio=="NONE":
    print("[BOOT] no gpio is selected. Automatically using PN532's gpios")
    use_PN532s_gpios=True
 
if use_PN532s_gpios:
    #TARGET_TIMEOUT_M=TARGET_TIMEOUT_M*2
    TARGET_TIMEOUT_Q=TARGET_TIMEOUT_Q*1.5

class message(object):
    def __init__(self, message_type, message_data):
        self.type = message_type
        self.data = message_data

class NFC_TAG(object):
    def __init__(self, tg, sens_res, sel_res, nfcid1):
        self.Tg = tg
        self.SENS_RES = sens_res
        self.SEL_RES = sel_res
        self.NFCID1 = nfcid1

class PARSING_STATUS(IntEnum):
    idle=0
    waiting_start_of_packet=1
    parsing_pkt_len=2   #this include LEN and LCS, here it also checks if the packet is an ACK/NACK
    parsing_pkt_data=3  #this includes TF, PAYLOAD and DCS
    waiting_postable=4
    error=5

def on_stdin_json_cb(jobj):
    try:
        params=jobj["params"]
        if params["readerAddress"]==reader_address:
            rgb_rpc=params["rgbColor"]
            rgb=[int(rgb_rpc["r"]),int(rgb_rpc["g"]),int(rgb_rpc["b"])]
            user_input_out_queue.put(message(USER_INPUT_THREAD.MessageType.set_rgb_led,rgb))
            #set_rgb_led(rgb)
        with print_lock:
            print("on_message: "+str(jobj))
    except Exception as e:
        with print_lock:
            print("Exception in on_message!: "+ str(e))

PN532_ack_wait=False
PN532_response_wait=False
PN532_last_comand=None

serial_lock=threading.RLock()
print_lock=threading.RLock()
serial_conf_lock=threading.RLock()

ser=serial.Serial()

def serial_tx_exec(data):
    global ser
    #try:
    ser.write(data)
    ser.flush()
    PN532_last_comand=data
    return True
    #except serial.serialutil.SerialException as e:
    #    with print_lock:
    #        print("[SERIAL] Exception during serial write! "+str(e))
    #        if ser.is_open:
    #            ser.close()
    #    return False


def PN532_send_comand(message,response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):
    MaxNoOfAttempts=4 #if ack is timed out the command is resent this amount of times
    for trial in range(MaxNoOfAttempts):
        PN532_clear_ack()
        with serial_lock:
            tx_ok = serial_tx_exec(message)
        if tx_ok:
            tx_time=time.time()
            if PN532_wait_ack(ack_timeout): #ack correctly received
                ack_time=time.time()
                return PN532_wait_response(response_timeout-(ack_time-tx_time)) #here we also compensate the actual delay between uart_tx and ack_rx
            else:
                #send_nack() #this aborts the previous comand in case it has been received and the ack took too much to be received
                pass
        else:   #if some exception has been risen by serial_tx_exec do not wait any response, just exit
            print("[SERIAL] Something bad happened!")
            break

    if tx_ok:
        with print_lock:
            print("[PN532] ACK timed out for "+str(MaxNoOfAttempts)+" times!")

    return False

def PN532_clear_ack():
    global PN532_ack_wait

    if PN532_ack_wait:
        with print_lock:
            print("[PN532] Ack event clean, but PN532_ack_wait was: True")
    
    ack_event.clear()
    PN532_ack_wait=False

def PN532_wait_ack(timeout=PN532_ack_timeout_value):
    global PN532_ack_wait

    PN532_ack_wait=True
    ack_event.wait(timeout)
    PN532_ack_wait=False
    if ack_event.isSet():
        ret=True
    else:
        ret=False
    ack_event.clear()
    return ret

def PN532_ack_handler():
    ack_event.set()
    #time.sleep(0.005)

def PN532_nack_handler():
    global PN532_ack_wait

    #PN532_ack_wait=False
def PN532_checksum_error_handler():
    send_nack()

def PN532_wait_response(timeout=PN532_response_timeout_value):
    global PN532_response_wait

    PN532_response_wait=True
    response_event.wait(timeout)
    PN532_response_wait=False
    if response_event.isSet():
        response_event.clear()
        return True
    else:
        #with print_lock:
        #    print("[PN532] last command's response timed out. No retries")
        return False

def prepare_output_frame(command):
    PREAMBLE=0x00
    TFI=0xD4
    leng=len(command)+1 #+1 because the len includes the TFI
    len_cs=(256-leng)%256
    c_sum= sum(command)+TFI
    data_cs=(256-(c_sum))%256
    POSTAMBLE=0x00

    frame=bytes([PREAMBLE]+START_OF_PACKET+[leng]+[len_cs]+[TFI]+command+[data_cs]+[POSTAMBLE])
    return frame

def send_ack():
    ack_message=bytes([0x00,0x00, 0xFF,0x00, 0xFF, 0x00])
    serial_tx_exec(ack_message)

def send_nack():
    #with print_lock:
    #    print("[PN532] sending nack")
    nack_message=bytes([0x00,0x00, 0xFF,0x00, 0x00])
    serial_tx_exec(nack_message)

def wake_up_reader_and_determine_baudrate():
    with print_lock:
        print("[PN532] Trying to determine baudrate")
    baudrates=[115200, 921600]
    for b in baudrates:
        try:
            while(ser.out_waiting): #wait any pending output to be sent for safety
                pass
            ser.baudrate=b
            if(wake_up_reader(1,0.5)): #for the wake up command increase the timouts because it is a critical command, and sometimes it takes longer
                with print_lock:
                    print("[PN532] Uart baudrate is: "+str(b))
                return b
        except Exception as e:
            traceback.print_tb(e.__traceback__)
            pass

    raise SystemError('Cannot determine baudrate!')

def wake_up_reader(response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):

    wake_up_reader=bytes([0x55, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0xFF,0x03, 0xfd, 0xd4, 0x14, 0x01, 0x17, 0x00])
    return PN532_send_comand(wake_up_reader,response_timeout,ack_timeout)
    #command=[0x14, 0x01, 0x14, 0x01]
    #PN532_send_comand(prepare_output_frame(command))

def power_down_reader(response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):

    command=[0x16, 0xFB]
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)

def read_tag(response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):

    #with print_lock:
    #    print("[PN532] Read tag")

    command=[0x4A, 0x02, 0x00]
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)

def read_gpios(response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):
    command=[0x0C]
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)

def set_gpios(P3, P7, response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):
    command=[0x0E, P3, P7]
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)

def get_firmware_version(response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):

    command=[0x02]
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)


def set_PN532_serial_baudrate(baudrate,response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):

    if baudrate==9600:
        BR=0x00
    elif baudrate==19200:
        BR=0x01
    elif baudrate==38400:
        BR=0x02
    elif baudrate==57600:
        BR=0x03
    elif baudrate==115200:
        BR=0x04
    elif baudrate==230400:
        BR=0x05
    elif baudrate==460800:
        BR=0x06
    elif baudrate==921600:
        BR=0x07
    elif baudrate==1288000:
        BR=0x08
    else:
        raise ValueError("baudrate isn't a valid baudrate!")

    command=[0x10, BR] #set baudrate to 921600 bps
    return PN532_send_comand(prepare_output_frame(command),response_timeout,ack_timeout)

def send_raw_commad(commad,response_timeout=PN532_response_timeout_value,ack_timeout=PN532_ack_timeout_value):
    return PN532_send_comand(prepare_output_frame(list(commad)),response_timeout,ack_timeout)

def write_data_to_target(tag,data): #TODO: here we miss the read address, for now hardcoded

    tag_type=1
    if tag_type==1: #mifare classic
        temp=bytes([0x40,tag.Tg,0x60,0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ])+int(tag.NFCID1,16).to_bytes(4, byteorder='big')
        mifare_authenticate_command=[x for x in temp]

        PN532_send_comand(prepare_output_frame(mifare_authenticate_command))

        address_int=0x04
        mifare_read_command=[0x40, tag.Tg, 0xa0, address_int]+data
        #with print_lock:
        #    print("[DEBUG] write command: "+str(mifare_read_command))
        PN532_send_comand(prepare_output_frame(mifare_read_command))

    elif tag_type==2:   #mifare ultralight
        message=[0x02]

    else:
        return False

    return True

def read_data_from_target(tag): #TODO: here we miss the read address, for now hardcoded

    tag_type=1
    if tag_type==1: #mifare classic
        temp=bytes([0x40,tag.Tg,0x60,0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ])+int(tag.NFCID1,16).to_bytes(4, byteorder='big')
        mifare_authenticate_command=[x for x in temp]

        PN532_send_comand(prepare_output_frame(mifare_authenticate_command))

        address_int=0x04
        mifare_read_command=[0x40, tag.Tg, 0x30, address_int]

        PN532_send_comand(prepare_output_frame(mifare_read_command))
    elif tag_type==2:   #mifare ultralight
        message=[0x02]
    else:
        return False
    return True

def to_byte(hex_text):
    return binascii.unhexlify(hex_text)

def to_text(bytes_data):
    return binascii.hexlify(bytes_data)

class UART_READ_THREAD(threading.Thread):
    def __init__(self, threadID, name,outqueue,response_event):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.__loop=True
        self.__outqueue=outqueue
        self.__response_event=response_event

    @unique
    class MessageType(IntEnum):
        target_detected=1
        serial_ready=2
        pn532_baudrate_changed=3
        gpio_status=4

    def stop(self):
        self.__loop=False

    def decode_gpio_read(self, frame_data):
        self.__outqueue.put(message(UART_READ_THREAD.MessageType.gpio_status,frame_data))

    def list_passive_targets_response_handler(self, frame_data):
        p=0
        number_of_targets=int.from_bytes([frame_data[p]],byteorder='big',signed=False)
        p+=1
        i=0
        while(p<(len(frame_data)-1) and i < number_of_targets):

            TG=int.from_bytes([frame_data[p]],byteorder='big',signed=False)
            p+=1

            SEL_RES_b=bytes([frame_data[p], frame_data[p+1]])
            SENS_RES_hs=SEL_RES_b.hex()
            p+=2

            SEL_RES_b=frame_data[p:p+1]
            SEL_RES_hs=SEL_RES_b.hex()
            p+=1

            NFCIDLength=int.from_bytes([frame_data[p]],byteorder='big',signed=False)
            p+=1

            NFCID1_b=frame_data[p:p+NFCIDLength]    #raw bytes
            NFCID1_hs=NFCID1_b.hex()                #hex formatted string
            NFCID1_i=int.from_bytes(NFCID1_b,byteorder='big',signed=False)
            p+=NFCIDLength

            self.__outqueue.put(message(UART_READ_THREAD.MessageType.target_detected,NFC_TAG(TG,SENS_RES_hs,SEL_RES_hs,NFCID1_hs)))
            i+=1

    def ch_frame_handler(self, frame_data):
        type = int.from_bytes([frame_data[0]],byteorder='big',signed=False)
        if type==0x4B:
            self.list_passive_targets_response_handler(frame_data[1:])
        elif type==0x11:
            self.__outqueue.put(message(UART_READ_THREAD.MessageType.pn532_baudrate_changed,None))
        elif type==0x15:
            with print_lock:
                print("[PACKET DECODE] SAMConfiguration response (typically in response of a wake up command).")
        elif type==0x0D:
            self.decode_gpio_read(frame_data[1:])
        elif type==0x0F: #event in response of writeGpio command
            pass
        else:
            with print_lock:
                print("[PACKET DECODE] unknown frame type: 0x%X frame: 0x%s" %(type,to_text(frame_data)))
            pass

    def frameHandler(self, packet_data_field):
        if packet_data_field[0]==0xD4: #host to controller frame
            # #print("[PACKET HANDLER] host to controller frame with data: "+str(packet_data_field[1:].hex()));
            send_ack()
        elif packet_data_field[0]==0xD5: #controller to host frame
            # #print("[PACKET HANDLER] controller to host frame with data: "+str(packet_data_field[1:].hex()));
            send_ack()
            self.ch_frame_handler(packet_data_field[1:])
            self.__response_event.set()
        else:
            with print_lock:
                print("[PACKET HANDLER] ERROR frame. Error code: "+str(packet_data_field));
            send_ack()

    def run(self):
        global ser

        buff=deque()
        parsing_status=PARSING_STATUS.waiting_start_of_packet
        errorShown=False
        reset_serial_port()
        while self.__loop:
            try:
                if ser.is_open:
                    #bytesWaiting = ser.in_waiting
                    #if bytesWaiting > 0:
                        if parsing_status==PARSING_STATUS.idle:
                            parsing_status=PARSING_STATUS.waiting_start_of_packet
                            continue
                        elif parsing_status==PARSING_STATUS.error:
                            parsing_status=PARSING_STATUS.waiting_start_of_packet
                            continue
                        elif parsing_status==PARSING_STATUS.waiting_start_of_packet:
                            #with print_lock:
                            #    print("bytesWaiting: "+str(bytesWaiting))
                            buff.append(int.from_bytes(ser.read(1),byteorder='big',signed=False))

                            if len(buff) < len(START_OF_PACKET):
                                buff.append(int.from_bytes(ser.read(1),byteorder='big',signed=False))
                            
                            if buff==deque(START_OF_PACKET):
                                buff.clear()
                                parsing_status=PARSING_STATUS.parsing_pkt_len
                                #with print_lock:
                                #   print("[PACKET DECODE] start detected!")
                                continue   
                            else:
                                if buff!=deque([0,0]):
                                    with print_lock:
                                        print("[PACKET DECODE] Warning. Non zero data discarded: "+str(buff))
                                buff.popleft()
                                continue
                        
                        elif parsing_status==PARSING_STATUS.parsing_pkt_len:
                            pkt_len=int.from_bytes(ser.read(1),byteorder='big',signed=False)
                            pkt_len_cs=int.from_bytes(ser.read(1),byteorder='big',signed=False)

                            #with print_lock:
                            #   print("[PACKET DECODE] pkt_len: " + str(pkt_len) +" pkt_len_cs: "+str(pkt_len_cs))


                            if(pkt_len==0x00 and pkt_len_cs==0xFF): #ACK frame
                                #with print_lock:
                                #   print("[PACKET DECODE] ACK frame received!")
                                parsing_status=PARSING_STATUS.waiting_postable
                                PN532_ack_handler()
                                continue

                            if(pkt_len==0xFF and pkt_len_cs==0x00): #NACK frame
                                with print_lock:
                                    print("[PACKET DECODE] NACK frame received!")
                                parsing_status=PARSING_STATUS.waiting_postable
                                PN532_nack_handler()
                                continue

                            if pkt_len==255 and pkt_len_cs==255: #Extended information frame #TODO: NOT IMPLEMENTED!!!
                                with print_lock:
                                    print("[PACKET DECODE] Extended information frame detected, not implemented yet")
                                parsing_status=PARSING_STATUS.idle
                                continue

                            if (pkt_len+pkt_len_cs)%256 == 0: #Packet length checksum OK
                                parsing_status=PARSING_STATUS.parsing_pkt_data
                                continue
                            else: #Packet length checksum ERROR
                                with print_lock:
                                    print("[PACKET DECODE] Len checksum ERROR")
                                parsing_status=PARSING_STATUS.error
                                PN532_checksum_error_handler()
                                continue

                        elif parsing_status==PARSING_STATUS.parsing_pkt_data:
                            data_field = ser.read(pkt_len)  #TODO: properly set and manage timeouts in case the function doesn't return in a given time

                            data_cs=int.from_bytes(ser.read(1),byteorder='big',signed=False)
                            #with print_lock:
                                #print("[PACKET DECODE] Data checksum: %d" % data_cs)
                            datasum=sum(data_field)
                            if (datasum+data_cs)%256 != 0:
                                with print_lock:
                                    print("[PACKET DECODE] Data checksum ERROR")
                                parsing_status=PARSING_STATUS.error
                                PN532_checksum_error_handler()
                                continue
                            else:
                                # #print("[PACKET DECODE] Data checksum OK")
                                parsing_status=PARSING_STATUS.waiting_postable
                                if len(data_field):
                                    self.frameHandler(data_field)
                                else:
                                    with print_lock:
                                        print("[PACKET DECODE] something wrong happened. data_field contains zero elements")
                                continue

                        elif parsing_status==PARSING_STATUS.waiting_postable:
                            buff.append(int.from_bytes(ser.read(1),byteorder='big',signed=False))
                            #time.sleep(0.01)
                            if buff==deque(POSTABLE):
                                #with print_lock:
                                    #print("[PACKET DECODE] Postamble OK")
                                parsing_status=PARSING_STATUS.idle
                                continue
                            else:
                                with print_lock:
                                    print("[PACKET DECODE] Unexpected postamble: "+ str(buff))
                                parsing_status=PARSING_STATUS.error
                            
                            if len(buff)!=1:
                                with print_lock:
                                    print("[PACKET DECODE] Warning: buff should contain only one element here. Instead it contains "+ str(len(buff)) +" elements.")

                            if buff[-1]==START_OF_PACKET[-1]:
                                buff.clear()
                                parsing_status=PARSING_STATUS.parsing_pkt_len
                                with print_lock:
                                   print("[PACKET DECODE] start detected in place of postamble!")
                                continue 

                        else:
                            parsing_status=PARSING_STATUS.error
                            continue

                else: # !ser.is_open (serial port is not open)
                    if not errorShown:
                        print('[UART] Serial Port closed! Trying to open port: %s'% ser.port)

                    #raise serial.serialutil.SerialException("The serial port is closed")

                    parsing_status=PARSING_STATUS.idle
                    
                    #reset_serial_port()

                    ser.open()
                    if ser.is_open:
                        errorShown=False
                        ser.reset_input_buffer()
                        ser.reset_output_buffer()
                        print("[UART] Serial Port open!")
                        self.__outqueue.put(message(UART_READ_THREAD.MessageType.serial_ready,True))

            except (serial.serialutil.SerialException, TypeError) as e:
                if not errorShown:
                    print("[UART] Serial Port open exception (I'll keep trying to open it!...).")
                    traceback.print_tb(e.__traceback__)
                    errorShown=True
                parsing_status=PARSING_STATUS.error
                reset_serial_port()
                self.__outqueue.put(message(UART_READ_THREAD.MessageType.serial_ready,False))
                time.sleep(0.1)

                #if ser.is_open:
                #    errorShown=False
                #    ser.reset_input_buffer()
                #    ser.reset_output_buffer()
                #    print("[UART] Serial Port open!")
                #    self.__outqueue.put(message(UART_READ_THREAD.MessageType.serial_ready,True))

                #parsing_status=PARSING_STATUS.idle
                #time.sleep(0.1)

                pass

        #except KeyboardInterrupt as k:
        #    raise

def reset_serial_port():
    global ser
    global serial_conf_lock
    with serial_conf_lock:
        try:
            if ser.is_open:
                ser.close()
        except UnboundLocalError:
            pass
        try:
            ser.__del__() 
        except UnboundLocalError:
            pass

        if use_PN532s_gpios:
            p=pexpect.spawn("rfcomm release 0")
            try:
                p.expect('\n')
                retStr=p.before
                print("[UART] rfcomm release 0\n"+str(retStr))
            except pexpect.exceptions.EOF:
                print("[UART] rfcomm release 0 -> EOF")
                pass
            rfcomm_bt_addr="88:6B:0F:25:38:4B"
            #p.sendline("rfcomm bind 0 "+rfcomm_bt_addr)
            time.sleep(0.1)
            p=pexpect.spawn("rfcomm bind 0 "+rfcomm_bt_addr)         
            try:
                p.expect('\n')
                retStr=p.before
                print("[UART] rfcomm bind 0 "+rfcomm_bt_addr+"\n"+str(retStr))
            except pexpect.exceptions.EOF:
                print("[UART] rfcomm bind 0 "+rfcomm_bt_addr+" -> EOF")
                pass            
            time.sleep(0.1)
                       
        ser = serial.Serial()
        ser.port = serial_port
        ser.baudrate = RESET_BAUDRATE
        ser.write_timeout = 5
        ser.timeout = 1
        ser.exclusive=True

class USER_INPUT_THREAD(threading.Thread):
    def __init__(self, threadID, name,outqueue):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True
        self.__outqueue=outqueue
    
    def stop(self):
        self.__loop=False

    def run(self):
        polling_enabled=True
        writeData=0x01
        lstat=0
        while self.__loop:
            input_str = input()
            try:
                jobj=json.loads(input_str)
                on_stdin_json_cb(jobj)
            except json.JSONDecodeError:
                try:
                    if len (input_str) == 1:
                        if input_str == 'r':
                            #print("[USER_INPUT] read tag")
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.read_tag,'None'))
                        elif input_str == 'w':
                            #print("[USER_INPUT] wake up the reader")
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.wake_up_reader,'None'))
                        elif input_str == 'd':
                            #print("[USER_INPUT] power down the reader")
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.power_down_reader,'None'))
                        elif input_str == 'f':
                            #print("[USER_INPUT] get firmware version")
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.get_firmware_version,'None'))
                        elif input_str == 's':
                            if polling_enabled:
                                polling_enabled=False
                                #print("[USER_INPUT] stop polling")
                                self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.disable_polling,'None'))
                            else:
                                polling_enabled=True
                                #print("[USER_INPUT] start polling")
                                self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.enable_polling,'None'))

                        elif input_str == 'b':
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.set_uart_baudrate,'None'))

                        elif input_str == 'g':
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.get_uart_baudrate,'None'))
                        elif input_str == 'p':
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.pubblish_fake_data,'None'))
                        elif input_str == 'l':
                            r=0
                            g=0
                            b=0
                            if lstat==0:
                                r=1
                            elif lstat==1:
                                r=1
                                g=1
                            elif lstat==2:
                                r=1
                                g=1
                                b=1
                            else:
                                lstat=-1
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.set_rgb_led,[r,g,b]))
                            lstat=lstat+1
                        else:
                            with print_lock:
                                print("[USER_INPUT] Invalid input. Read data: "+ input_str)
                    elif len (input_str) > 1:
                        if input_str[0] == 'c':
                            command_data=to_byte(input_str[1:])
                            #print("comand data: "+str(command_data))
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.raw_command,command_data))

                        elif input_str[0] == 'w':
                            nfcid1=to_byte("baa20b05").hex()
                            data_to_write=[writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData, writeData]#to_byte(input_str[1:])
                            writeData+=1
                            request_data=dict()
                            request_data["nfcid1"]=nfcid1
                            request_data["data_to_write"]=data_to_write
                            #print("comand data: "+str(command_data))
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.write_request,request_data))

                        elif input_str[0] == 'r':
                            nfcid1=to_byte("baa20b05").hex()
                            #data_to_write=to_byte(input_str[1:])
                            request_data=dict()
                            request_data["nfcid1"]=nfcid1
                            #request_data["data_to_write"]=data_to_write
                            #print("comand data: "+str(command_data))
                            self.__outqueue.put(message(USER_INPUT_THREAD.MessageType.read_request,request_data))
                    else:
                        with print_lock:
                            print("[USER_INPUT] Invalid input. Read data: "+ input_str)

                except ValueError:
                    with print_lock:
                        print("[USER_INPUT] Read failed. Read data: "+ input_str)

    @unique
    class MessageType(IntEnum):
        enable_polling=1
        disable_polling=2
        read_tag=3
        wake_up_reader=4
        power_down_reader=5
        get_firmware_version=6
        raw_command=7
        write_request=8
        read_request=9
        set_uart_baudrate=10
        get_uart_baudrate=11
        pubblish_fake_data=12
        set_rgb_led=13

class POLL_READER_THREAD(threading.Thread):
    @unique
    class MessageType(IntEnum):
        write_gpio_request=1

    def __init__(self, threadID, name, in_queue):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True
        self.__in_queue=in_queue

    def stop(self):
        self.__loop=False

    def run(self):
        while self.__loop:
            try:
                if not read_tag():  #timeout for N times (N=4 now..)
                    #raise TimeoutError
                    pass
                if use_PN532s_gpios:
                    #read_gpios()
                    try:
                        msg=self.__in_queue.get(False)
                        msg_type=msg.type
                        msg_data=msg.data
                        if msg_type==self.MessageType.write_gpio_request:
                            set_rgb_led(msg_data)
                        else:
                            with print_lock:
                                print("Runtime error: message type not recognized!")

                        self.__in_queue.task_done()
                    except queue.Empty as e:
                        pass
                    
                time.sleep(0.01)
            except (serial.serialutil.SerialException) as e:
                with print_lock:
                    print("Exception in the polling thread, stopping it!")
                traceback.print_tb(e.__traceback__)
                stop_PN532_polling()
                reset_serial_port()
                pass

class CHECK_AND_PLOT_TARGETS_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def stop(self):
        self.__loop=False

    def run(self):
        #global TARGET_TIMEOUT_VALUE_DEF
        while self.__loop:
            time.sleep(0.05) #this makes the cpu usage to drop, make it smaller if needed, but do not remove!
            targetsToRemove=[]

            if TARGET_TIMEOUT_VALUE_DEF == None:
                TARGET_TIMEOUT_VALUE=TARGET_TIMEOUT_Q+TARGET_TIMEOUT_M*len(targetsInRange)
            else:
                TARGET_TIMEOUT_VALUE=TARGET_TIMEOUT_VALUE_DEF
            #print("Timeout: %.3f" %TARGET_TIMEOUT_VALUE)
            with dicLock:
                publish_data=[]
                for t in targetsInRange:
                    now=time.time()
                    if now-targetsInRange[t]>TARGET_TIMEOUT_VALUE:
                        targetsToRemove.append(t)
                for r in targetsToRemove:
                    with print_lock: # lock can be disabled to avoid locks during sensitive operation.
                        print("[EVENT] Removed target: 0x%s. Total of: %d targets." %(r, len(targetsInRange)-1))
                    publish_data.append({"NFCID1":r,"status":"REMOVED"})
                    targetsInRange.pop(r)
                
            for p in publish_data:  #this is to avoid calling dump_json_stdout with dicLock
                dump_json_stdout(p)

def start_PN532_polling():
    global poll_reader_thread
    global check_plot_targets_thread
    global pollingEnabled
    global poll_reader_in_queue

    with print_lock:
        print("[APPLICATION] Start PN532 polling.")

    if poll_reader_thread!=None:
        poll_reader_thread.stop()

    poll_reader_thread=POLL_READER_THREAD(3,"poll reader thread",poll_reader_in_queue)
    poll_reader_thread.start()

    #check_plot_targets_thread=CHECK_AND_PLOT_TARGETS_THREAD(4,"check and plot targets thread")
    #check_plot_targets_thread.start()

    pollingEnabled=True

def stop_PN532_polling():
    global poll_reader_thread
    global check_plot_targets_thread
    global pollingEnabled


    with print_lock:
        print("[APPLICATION] Stop PN532 polling.")

    poll_reader_thread.stop()
    #check_plot_targets_thread.stop()
    pollingEnabled=False

def dump_json_stdout(data_to_publish):
    global mqtt_client
    global mqtt_connection_active

    data_to_publish["readerAddress"]=reader_address

    with print_lock:
        print(json.dumps(data_to_publish))


def gpio_change_handler(gpio_or_val):
    if not use_PN532s_gpios:
        if gpio_or_val==button_gpio:
            input_val=GPIO.input(button_gpio)
            publish_data={"BUTTON":input_val}
            dump_json_stdout(publish_data)
            with print_lock:
                print("[EVENT] "+button_gpio+" changed to -> "+str(input_val))
        else:
            with print_lock:
                print("[EVENT] event not on the IN_GPIO: "+button_gpio+" but on: "+gpio)
    else:
        publish_data={"BUTTON":gpio_or_val}
        dump_json_stdout(publish_data)
        print("[EVENT] PN532 button changed to -> "+str(gpio_or_val))

gpio_ready=False
def GPIO_setup():
    global gpio_ready
    try:
        if button_gpio!=None and button_gpio!="NONE":
            p=pexpect.spawn("config-pin "+button_gpio+" gpio")
            p.expect(pexpect.EOF)
        if button_gpio!=None and led_red_gpio!="NONE":
            p=pexpect.spawn("config-pin "+led_red_gpio+" gpio")
            p.expect(pexpect.EOF)
        if button_gpio!=None and led_green_gpio!="NONE":
            p=pexpect.spawn("config-pin "+led_green_gpio+" gpio")
            p.expect(pexpect.EOF)
        if button_gpio!=None and led_blue_gpio!="NONE":
            p=pexpect.spawn("config-pin "+led_blue_gpio+" gpio")
            p.expect(pexpect.EOF)

        GPIO.setup(button_gpio,GPIO.IN,pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(button_gpio, GPIO.BOTH, gpio_change_handler, 5)

        GPIO.setup(led_red_gpio, GPIO.OUT, initial=0)
        GPIO.setup(led_green_gpio, GPIO.OUT, initial=0)
        GPIO.setup(led_blue_gpio, GPIO.OUT, initial=0)
        gpio_ready=True
        with print_lock:
            print("[APPLICATION] GPIOs ready!")

    except Exception as e:
        with print_lock:
            print("[EVENT] Exception during GPIO initialization. " + str(e))
        pass
        gpio_ready=False

def set_rgb_led(rgb):
    if not use_PN532s_gpios:
        if gpio_ready:
            r=rgb[0]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
            g=rgb[1]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
            b=rgb[2]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
            GPIO.output(led_red_gpio,r)
            GPIO.output(led_green_gpio,g)
            GPIO.output(led_blue_gpio,b)
        else:
            with print_lock:
                print("[APPLICATION] gpios not ready, cannot change it!")
    else:  
        r=rgb[0]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
        g=rgb[1]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
        b=rgb[2]!=0     #this to convert to 0-1 value, rgb leds are not connected to pwm then only on-off value can be used
        P3=0 | (1<<7) | int(b)<<3 | int(g)<<2 | int(r)<<1 
        P7=0
        set_gpios(P3,P7)

def led_fade():
    fade_interval=0.1
    if not use_PN532s_gpios:
        rgb=[1,0,0]
        set_rgb_led(rgb)
        time.sleep(fade_interval)
        rgb=[0,1,0]
        set_rgb_led(rgb)
        time.sleep(fade_interval)
        rgb=[0,0,1]
        set_rgb_led(rgb)
        time.sleep(fade_interval)
        rgb=[1,1,1]
        set_rgb_led(rgb)
        time.sleep(1)
        rgb=[0,0,0]
        set_rgb_led(rgb)
    else:
        rgb=[1,0,0]
        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
        rgb=[0,1,0]
        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
        rgb=[0,0,1]
        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
        rgb=[1,1,1]
        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
        rgb=[0,0,0]
        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
        

def signal_handler(sig, frame):
    clean_exit()

signal.signal(signal.SIGINT, signal_handler)

GPIO_setup()
if use_PN532s_gpios:
    button_prev_state=True

targetsInRange=dict()
dicLock=threading.Lock()
pollingEnabled=False

response_event=threading.Event()
ack_event=threading.Event()

uart_read_out_queue=queue.Queue()
uart_read_thread=UART_READ_THREAD(1,"uart read thread",uart_read_out_queue,response_event,)
uart_read_thread.setDaemon(True)
uart_read_thread.start()

user_input_out_queue=queue.Queue()
user_input_thread=USER_INPUT_THREAD(2,"user input thread",user_input_out_queue)
user_input_thread.setDaemon(True)
user_input_thread.start()

poll_reader_in_queue=queue.Queue()
poll_reader_thread=POLL_READER_THREAD(3,"poll reader thread",poll_reader_in_queue)

check_plot_targets_thread=CHECK_AND_PLOT_TARGETS_THREAD(4,"check and plot targets thread")
check_plot_targets_thread.start()

#write_request_pending=False
write_requests=deque() #this is a list, used as a queue
read_requests=deque() #this is a list, used as a queue

fake_data_status=True
boot_ok=False
led_faded=False
while 1:
    try:
        try:
            msg=uart_read_out_queue.get(False)
            if msg.type==UART_READ_THREAD.MessageType.serial_ready:
                with print_lock:
                    print("[EVENT] serial port ready: "+str(msg.data))
                if msg.data:
                    if AUTOSTART:
                        try:
                            wake_up_reader_and_determine_baudrate()
                            if(ser.baudrate==runtime_baudrate):
                                boot_ok=True
                                start_PN532_polling()
                                pass
                            else:
                                with print_lock:
                                    print("[APPLICATION] Sending request for new uart baudrate: "+str(runtime_baudrate))
                                set_PN532_serial_baudrate(runtime_baudrate, 1, 0.5) #once this is executed it will generate pn532_baudrate_changed, the polling can be enabled after that
                            pass
                        except SystemError as e:
                            boot_ok=False
                            with print_lock:
                                print("[APPLICATION] Error while determining baudrate. "+str(e))
                            pass
                else:   #the reader stoped working
                    led_faded=False
                    boot_ok=False
                    stop_PN532_polling()

            elif msg.type==UART_READ_THREAD.MessageType.pn532_baudrate_changed:
                with print_lock:
                    print("[PACKET DECODE] Change uart baudrate response! Going to change it locally now to: "+str(runtime_baudrate))
                while(ser.out_waiting): #wait any pending output to be sent for safety
                    pass
                ser.baudrate=runtime_baudrate
                boot_ok=True
                if AUTOSTART:
                    start_PN532_polling()

            elif msg.type==UART_READ_THREAD.MessageType.target_detected:

                tag=msg.data
                prev_pollingEnabled=pollingEnabled
                if len(write_requests)>0:
                    if tag.NFCID1 == write_requests[0]["nfcid1"]:
                        if prev_pollingEnabled:
                            stop_PN532_polling()
                        if write_data_to_target(tag, write_requests[0]["data_to_write"]):
                            write_requests.popleft()    #remove the write request from the queue
                        # restart the sensor polling (if enabled)
                        if prev_pollingEnabled:
                            start_PN532_polling()
                    pass

                if len(read_requests)>0:
                    if tag.NFCID1 == read_requests[0]["nfcid1"]:
                        if prev_pollingEnabled:
                            stop_PN532_polling()
                        if read_data_from_target(tag):
                            read_requests.popleft()    #remove the write request from the queue
                        # restart the sensor polling (if enabled)
                        if prev_pollingEnabled:
                            start_PN532_polling()
                    pass

                if pollingEnabled:
                    publish=False
                    with dicLock:
                        if tag.NFCID1 not in targetsInRange:
                            publish_data={"NFCID1":str(tag.NFCID1),"status":"ADDED"}
                            publish=True
                            with print_lock: # lock can be disabled to avoid locks during sensitive operation.
                                print("[EVENT] Added target. Tg: %d, NFCID1: 0x%s, SENS_RES: 0x%s, SEL_RES: 0x%s. Total of: %d targets." %(tag.Tg, tag.NFCID1, tag.SENS_RES, tag.SEL_RES, len(targetsInRange)+1))
                        targetsInRange[tag.NFCID1]=time.time()
                    if publish: #this is to avoid calling dump_json_stdout with dicLock
                        dump_json_stdout(publish_data)
                else:
                    with print_lock:
                        print("[APPLICATION] Target detected. Tg: %d,NFCID1: 0x%s, SENS_RES: 0x%s, SEL_RES: 0x%s" %(tag.Tg, tag.NFCID1, tag.SENS_RES, tag.SEL_RES))
            elif msg.type==UART_READ_THREAD.MessageType.gpio_status:
                #P3=msg.data[0]
                P7=msg.data[1]
                #I0I1=msg.data[2]
                #P3_0=P3 & 1
                #P3_1=(P3>>1) & 1
                #P3_2=(P3>>2) & 1
                #P3_3=(P3>>3) & 1
                #P3_4=(P3>>4) & 1
                #P3_5=(P3>>5) & 1

                #P7_0=P7 & 1
                #P7_1=(P7>>1) & 1
                P7_2=((P7>>2) & 1)
                button_state=P7_2==1 #==1 used for converting into boolean
                if button_state != button_prev_state:
                    gpio_change_handler(P7_2)
                    button_prev_state=button_state
            else:
                with print_lock:
                    print("[APPLICATION] Unknown data")

            uart_read_out_queue.task_done()
        except queue.Empty as e:
            pass

        try:
            msg=user_input_out_queue.get(False)
            if boot_ok:
                if msg.type==USER_INPUT_THREAD.MessageType.enable_polling:
                    with print_lock:
                        print("[APPLICATION] Enable polling")
                    start_PN532_polling()
                elif msg.type==USER_INPUT_THREAD.MessageType.disable_polling:
                    with print_lock:
                        print("[APPLICATION] Disable polling")
                    stop_PN532_polling()
                elif msg.type==USER_INPUT_THREAD.MessageType.read_tag:
                    with print_lock:
                        print("[APPLICATION] read tag")
                    read_tag()
                elif msg.type==USER_INPUT_THREAD.MessageType.wake_up_reader:
                    with print_lock:
                        print("[APPLICATION] wake up the reader")
                    wake_up_reader()
                elif msg.type==USER_INPUT_THREAD.MessageType.power_down_reader:
                    with print_lock:
                        print("[APPLICATION] power down the reader")
                    power_down_reader()
                elif msg.type==USER_INPUT_THREAD.MessageType.get_firmware_version:
                    with print_lock:
                        print("[APPLICATION] get firmware version")
                    get_firmware_version()
                elif msg.type==USER_INPUT_THREAD.MessageType.raw_command:
                    command_data=msg.data
                    with print_lock:
                        print("[APPLICATION] raw command: 0x"+command_data.hex())
                    send_raw_commad(command_data)
                elif msg.type==USER_INPUT_THREAD.MessageType.write_request:
                    write_arguments=msg.data
                    write_NFCID1=write_arguments["nfcid1"]
                    write_data=write_arguments["data_to_write"]
                    write_requests.append(write_arguments)
                    with print_lock:
                        print("[APPLICATION] write request on NFCID1: 0x%s, data to write: 0x%s" %(write_NFCID1, write_data))
                elif msg.type==USER_INPUT_THREAD.MessageType.read_request:
                    read_arguments=msg.data
                    read_NFCID1=read_arguments["nfcid1"]
                    read_requests.append(read_arguments)
                    with print_lock:
                        print("[APPLICATION] read request on NFCID1: 0x%s" %(read_NFCID1) )
                elif msg.type==USER_INPUT_THREAD.MessageType.set_uart_baudrate:
                    with print_lock:
                        print("[APPLICATION] Setting the new baudrate to PN532")
                    set_PN532_serial_baudrate(runtime_baudrate)
                elif msg.type==USER_INPUT_THREAD.MessageType.get_uart_baudrate:
                    with print_lock:
                        print("[APPLICATION] Determining uart baudrate")
                    try:
                        wake_up_reader_and_determine_baudrate()
                    except SystemError as e:
                        with print_lock:
                            print("[APPLICATION] Error while determining baudrate. "+str(e))
                        pass
                elif msg.type==USER_INPUT_THREAD.MessageType.pubblish_fake_data: 
                    with print_lock:
                        print("[APPLICATION] Publishing fake data")
                    if fake_data_status:
                        publish_data={"NFCID1":"123456789","status":"ADDED"}
                        dump_json_stdout(publish_data)
                        fake_data_status=False
                    else:
                        publish_data={"NFCID1":"123456789","status":"REMOVED"}
                        dump_json_stdout(publish_data)
                        fake_data_status=True
                elif msg.type==USER_INPUT_THREAD.MessageType.set_rgb_led:
                    rgb=msg.data
                    with print_lock:
                        print("[APPLICATION] Turning on RGB led. RGB: "+str(rgb))

                    if not use_PN532s_gpios: 
                        set_rgb_led(rgb)
                    else:
                        poll_reader_in_queue.put(message(POLL_READER_THREAD.MessageType.write_gpio_request,rgb))
                else:
                    with print_lock:
                        print("[APPLICATION] Unknown command")
            else:                
                print("[APPLICATION] device not ready, cannot accept command")

            user_input_out_queue.task_done()
        except queue.Empty as e:
            pass
        except serial.serialutil.SerialException as e:
            with print_lock:
                print("Exception in the main thread, I'll stop polling and reinitialize serial port.")
            stop_PN532_polling()
            reset_serial_port()
            pass

        if boot_ok and not led_faded:
            led_fade()
            led_faded=True
        #try:
        #    mqtt_client.loop()
        #except Exception as e:
        #    print("[APPLICATION] Exception in mqttclient.loop(). e: "+str(e))

        time.sleep(0.01) #this makes the cpu usage to drop, make it smaller if needed, but do not remove!
    except (KeyboardInterrupt, SystemExit):
        clean_exit()
        

def clean_exit():
    stop_PN532_polling()
    uart_read_thread.stop()   #NB: threads locked (not with a time.sleep) are not stopped in this way...
    user_input_thread.stop()   #NB: threads locked (not with a time.sleep) are not stopped in this way...
    check_plot_targets_thread.stop()  #NB: threads locked (not with a time.sleep) are not stopped in this way...
    
    poll_reader_thread.join(2)
    print("[APPLICATION] poll_reader_thread closed")
    uart_read_thread.join(2)
    print("[APPLICATION] uart_read_thread closed")
    user_input_thread.join(2)
    print("[APPLICATION] user_input_thread closed")
    check_plot_targets_thread.join(2)
    print("[APPLICATION] check_plot_targets_thread closed")

    try:
        if ser.is_open:
            ser.close()
    except UnboundLocalError:
        pass
    try:
        ser.__del__() 
    except UnboundLocalError:
        pass

    if use_PN532s_gpios:
        p=pexpect.spawn("rfcomm release 0")
        try:
            p.expect('\n')
            retStr=p.before
            print("[UART] rfcomm release 0\n"+str(retStr))
        except pexpect.exceptions.EOF:
            pass

    print("[APPLICATION] Properly closed")
    sys.exit()
    

#TODO: add MIFARE write support:
#
# 1) run InListPassiveTargets:
#                                       4a                                  InListPassiveTargets command
#                                       02                                  number of cards
#                                       00                                  baudrate
#   save Tg and NFCID1 of the desired card
# For mifare classic (ID length 4bytes):
#   2a) run InDataExchange with MiFare cmd code 60 (authenticate)
#                                       40                                  InDataExchange command
#                                       01                                  Tg number of the desired card
#                                       60                                  Mifare command (authenticate)
#                                       07                                  Mifare address TODO: not clear what this is
#                                       FF FF FF FF FF FF NFCID1(4bytes)    Authentication key
#   3a) run InDataExchange with MiFare cmd code A0 (write 16bytes)
#                                       40                                  InDataExchange command
#                                       01                                  Tg number of the desired card
#                                       a0                                  Mifare command (write 16bytes)
#                                       04                                  Mifare address TODO: not clear how addresses are organized in the memory
#                                       DATA(16 bytes)                      Data to be written
#   4a) repeat step 3a if needed
# For mifare ultralight (ID length 7bytes)
#   2b) run InDataExchange with MiFare cmd code A2 (write 4bytes)
#                                       40                                  InDataExchange command
#                                       01                                  Tg number of the desired card
#                                       a2                                  Mifare command (write 4bytes)
#                                       04                                  Mifare address TODO: not clear how addresses are organized in the memory
#                                       DATA(4 bytes)                       Data to be written
