Control script for the PN532 NFC reader

This python script controls the PN532 NFC reader connected over serial interface.
It works with python3 and it needs some packages that are not installed by default.
To install them:
	python3 -m pip install paho-mqtt
	python3 -m pip install pyserial
	python3 -m pip install pexpect
	python3 -m pip install Adafruit_BBIO

Once started it keeps polling the sensor to always keep an updated list of passive targets in the range of the reader.
Every time a new NFC card enters the reader's range an ADDED event is generated and pushed std_out as json object, when the card is removed a REMOVED event generated.
To run the script use python3:

	python3 main.py /dev/ttyS0 115200 nfc_reader_1 P8_45 P8_43 P8_41 P8_39

Where:

python3 main.py             : is the command to run the python script into the python3 intepreter (the 'python3' command might be just 'python')
/dev/ttyS0                  : serial port name (on windows this will be something like 'COM1')
115200			   			: serial baudrate. Set this value to 0 to leave the default one. 115200 and 921600 are tested to be working
nfc_reader_1		   	    : reader address. This string will be published together with any other data that is published by this process
P8_45 P8_43 P8_41 P8_39		: these are the GPIOs used by led and button in this order <button_gpio> <red_led_gpio> <green_led_gpio> <blue_led_gpio>. They can be "NONE" if not used.

Only arguments at the end can be dropped, and they will take the default value which are those used as example above.

The script can also control the same kind of reader connected with a BLUETOOTH rfcomm. The support is still preliminary, however it works. To launch it use something like:
	python3 main.py /dev/rfcomm0 115200 wireless_reader NONE NONE NONE NONE
	
Note: when the rfcomm is used, the pairing of Bluetooth should be performed using the bluetoothctl tool (sudo bluetoothctl in terminal), and then the MAC address of the device should be copied inside PN532/python_PN532/main.py (search for rfcomm_bt_addr and assign the MAC address of the paired device).
Note2: the Bluetooth version have a fixed routing for GPIOs since it uses PN532's gpio, then in the command line just use NONE for gpios as shown above.
Note3: by default the button for the Bluetooth version is disabled, to enable it just uncomment read_gpios() inside POLL_READER_THREAD (aprox line 816).

While running the script accept some commands from the keyboard. Each input command needs to be terminated with 'Enter'. Valid commands are listed here (others are available, but they are only for debugging UART connection with the PN532, take a look at the sources in the part on USER_INPUT_THREAD):

l : change led value (at any press a different color is shown)
p : publish dummy data
cXX..XX : can be used to send the XX..XX command to the PN532. XX are exadecimal representation of bytes. For instance, the InListPassiveTargets command can be sent with 'c4a0200'
wXX..XX : will be used to write data on a NFC memory, still work in progress, do not use it
rXX..XX : will be used to read data from a NFC memory, still work in progress, do not use it

