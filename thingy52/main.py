from bluepy.btle import UUID, Peripheral, ADDR_TYPE_RANDOM, DefaultDelegate, BTLEDisconnectError
import argparse
import time
import struct
import binascii
import paho.mqtt.client as mqtt
import json
import time
import uuid
import traceback
from collections import deque
import threading
import datetime

# Definition of all UUID used by Thingy
CCCD_UUID = 0x2902

BATTERY_SERVICE_UUID = 0x180F
BATTERY_LEVEL_UUID = 0x2A19

ENVIRONMENT_SERVICE_UUID = 0x0200
E_TEMPERATURE_CHAR_UUID = 0x0201
E_PRESSURE_CHAR_UUID    = 0x0202
E_HUMIDITY_CHAR_UUID    = 0x0203
E_GAS_CHAR_UUID         = 0x0204
E_COLOR_CHAR_UUID       = 0x0205
E_CONFIG_CHAR_UUID      = 0x0206

USER_INTERFACE_SERVICE_UUID = 0x0300
UI_LED_CHAR_UUID            = 0x0301
UI_BUTTON_CHAR_UUID         = 0x0302
UI_EXT_PIN_CHAR_UUID        = 0x0303

MOTION_SERVICE_UUID         = 0x0400
M_CONFIG_CHAR_UUID          = 0x0401
M_TAP_CHAR_UUID             = 0x0402
M_ORIENTATION_CHAR_UUID     = 0x0403
M_QUATERNION_CHAR_UUID      = 0x0404
M_STEP_COUNTER_UUID         = 0x0405
M_RAW_DATA_CHAR_UUID        = 0x0406
M_EULER_CHAR_UUID           = 0x0407
M_ROTATION_MATRIX_CHAR_UUID = 0x0408
M_HEAIDNG_CHAR_UUID         = 0x0409
M_GRAVITY_VECTOR_CHAR_UUID  = 0x040A

SOUND_SERVICE_UUID          = 0x0500
S_CONFIG_CHAR_UUID          = 0x0501
S_SPEAKER_DATA_CHAR_UUID    = 0x0502
S_SPEAKER_STATUS_CHAR_UUID  = 0x0503
S_MICROPHONE_CHAR_UUID      = 0x0504

# Notification handles used in notification delegate
e_temperature_handle = None
e_pressure_handle = None
e_humidity_handle = None
e_gas_handle = None
e_color_handle = None
ui_button_handle = None
m_tap_handle = None
m_orient_handle = None
m_quaternion_handle = None
m_stepcnt_handle = None
m_rawdata_handle = None
m_euler_handle = None
m_rotation_handle = None
m_heading_handle = None
m_gravity_handle = None
s_speaker_status_handle = None
s_microphone_handle = None

#MQTT ROOT TOPICS
TELEMETRY_TOPIC="telemetry"
RPC_REQUEST_TOPIC="rpc/request"
RPC_RESPONSE_TOPIC="rpc/response"

#MQTT DEVICE TOPIC
THINGY_TOPIC="" #the BD address (MAC address) will be put here as soon as the program is started

#MQTT BLE SERVICES TOPIC
ENVIRONMENT_SERVICE_TOPIC="environment"
UI_SERVICE_TOPIC="ui"
MOTION_SERVICE_TOPIC="motion"
SOUND_SERVICE_TOPIC="sound"
THINGY_CONF_SERVICE_TOPIC="thingy_configuration"

#MQTT BLE CHARACTERISTIC TOPIC
TEMPERATURE_CHARACTERISTIC_TOPIC="temperature"
PRESSURE_CHARACTERISTIC_TOPIC="pressure"
HUMIDITY_CHARACTERISTIC_TOPIC="humidity"
GAS_CHARACTERISTIC_TOPIC="gas"
COLOR_CHARACTERISTIC_TOPIC="color"
BUTTON_CHARACTERISTIC_TOPIC="button"
LED_CHARACTERISTIC_TOPIC="led"
TAP_CHARACTERISTIC_TOPIC="tap"
ORIENTATION_CHARACTERISTIC_TOPIC="orientation"
QUATERNION_CHARACTERISTIC_TOPIC="quaternion"
STEPCOUNTER_CHARACTERISTIC_TOPIC="step_counter"
RAWDATA_CHARACTERISTIC_TOPIC="raw_data"
EULER_CHARACTERISTIC_TOPIC="euler"
ROTATION_CHARACTERISTIC_TOPIC="rotation_matrix"
HEADING_CHARACTERISTIC_TOPIC="heading"
GRAVITY_CHARACTERISTIC_TOPIC="gravity_vector"
SPEAKER_CHARACTERISTIC_TOPIC="speaker_status"
BATTERY_LEVEL_CHARACTERISTIC_TOPIC="battery"

#MQTT CONFIGURATION
MQTT_BROKER_DEF="i3app-rabbit.fbk.eu" #"iot.smartcommunitylab.it"
MQTT_PORT_DEF=1883
DEVICE_TOKEN_DEF="test_vhost:i3usr"  #"MnAZcwSpqG3XjGO4RxJy" #FIRST 4 READERS PROTOTYPE: "0alJMWpXlOcY8bZuWxvK", SECOND 4 READERS PROTOTYPE: "B43vCbweCWihtUCn6vln", BLUETOOTH PROTOTYPE: MnAZcwSpqG3XjGO4RxJy
DEVICE_PASSWORD_DEF="aaa" #"None"
DEVICE_READABLE_NAME="Not assigned"

mqtt_broker=MQTT_BROKER_DEF
mqtt_port=MQTT_PORT_DEF
device_token=DEVICE_TOKEN_DEF
device_password=DEVICE_PASSWORD_DEF #shall this be added to command line arguments?

#ERRORS
SUCCESS=0   #everithing ok
UNKNOWN_SERVICE=1 #the characteristic is unknwonw
UNKNOWN_CHARACTERISTIC=2 #the characteristic is unknwonw
UNKNOWN_METHOD=3 #the parameter is unknwonw
VALUE_ERROR=4 #the value is not valid (i.e. the interval must be within 1 and 10 second, but 0.1 was requested)
UNKNOWN_ERROR=5 #the value is not valid (i.e. the interval must be within 1 and 10 second, but 0.1 was requested)

#OTHER GLOBAL VARIABLES
DEFAULT_MOTION_FREQ=5

################################################  HELPERS  #############################################
epoch = datetime.datetime.utcfromtimestamp(0)

def unix_time_millis(dt):
    return (dt - epoch).total_seconds() * 1000.0

def write_uint16(data, value, index):
    """ Write 16bit value into data string at index and return new string """
    data = data.decode('utf-8')  # This line is added to make sure both Python 2 and 3 works
    return '{}{:02x}{:02x}{}'.format(
                data[:index*4], 
                value & 0xFF, value >> 8, 
                data[index*4 + 4:])

def write_uint8(data, value, index):
    """ Write 8bit value into data string at index and return new string """
    data = data.decode('utf-8')  # This line is added to make sure both Python 2 and 3 works
    return '{}{:02x}{}'.format(
                data[:index*2], 
                value, 
                data[index*2 + 2:])

# Please see # Ref https://nordicsemiconductor.github.io/Nordic-Thingy52-FW/documentation
# for more information on the UUIDs of the Services and Characteristics that are being used
def Nordic_UUID(val):
    """ Adds base UUID and inserts value to return Nordic UUID """
    return UUID("EF68%04X-9B35-4933-9B10-52FFA9740042" % val)

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

def int_to_bytes(value, length):
    result = []
    for i in range(0, length):
        result.append(value >> (i * 8) & 0xff)
    result.reverse()
    return result


################################################  BLE  #############################################

class BatterySensor():
    """
    Battery Service module. Instance the class and enable to get access to Battery interface.
    """
    svcUUID = UUID(BATTERY_SERVICE_UUID)  # Ref https://www.bluetooth.com/specifications/gatt/services 
    dataUUID = UUID(BATTERY_LEVEL_UUID) # Ref https://www.bluetooth.com/specifications/gatt/characteristics

    def __init__(self, periph):
        self.periph = periph
        self.service = None
        self.data = None

    def enable(self):
        """ Enables the class by finding the service and its characteristics. """
        if self.service is None:
            self.service = self.periph.getServiceByUUID(self.svcUUID)
        if self.data is None:
            self.data = self.service.getCharacteristics(self.dataUUID)[0]

    def read(self):
        """ Returns the battery level in percent """
        val = ord(self.data.read())
        return val


class EnvironmentService():
    """
    Environment service module. Instance the class and enable to get access to the Environment interface.
    """
    serviceUUID =           Nordic_UUID(ENVIRONMENT_SERVICE_UUID)
    temperature_char_uuid = Nordic_UUID(E_TEMPERATURE_CHAR_UUID)
    pressure_char_uuid =    Nordic_UUID(E_PRESSURE_CHAR_UUID)
    humidity_char_uuid =    Nordic_UUID(E_HUMIDITY_CHAR_UUID)
    gas_char_uuid =         Nordic_UUID(E_GAS_CHAR_UUID)
    color_char_uuid =       Nordic_UUID(E_COLOR_CHAR_UUID)
    config_char_uuid =      Nordic_UUID(E_CONFIG_CHAR_UUID)

    def __init__(self, periph):
        self.periph = periph
        self.environment_service = None
        self.temperature_char = None
        self.temperature_cccd = None
        self.pressure_char = None
        self.pressure_cccd = None
        self.humidity_char = None
        self.humidity_cccd = None
        self.gas_char = None
        self.gas_cccd = None
        self.color_char = None
        self.color_cccd = None
        self.config_char = None

    def enable(self):
        """ Enables the class by finding the service and its characteristics. """
        global e_temperature_handle
        global e_pressure_handle
        global e_humidity_handle
        global e_gas_handle
        global e_color_handle

        if self.environment_service is None:
            self.environment_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.temperature_char is None:
            self.temperature_char = self.environment_service.getCharacteristics(self.temperature_char_uuid)[0]
            e_temperature_handle = self.temperature_char.getHandle()
            self.temperature_cccd = self.temperature_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.pressure_char is None:
            self.pressure_char = self.environment_service.getCharacteristics(self.pressure_char_uuid)[0]
            e_pressure_handle = self.pressure_char.getHandle()
            self.pressure_cccd = self.pressure_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.humidity_char is None:
            self.humidity_char = self.environment_service.getCharacteristics(self.humidity_char_uuid)[0]
            e_humidity_handle = self.humidity_char.getHandle()
            self.humidity_cccd = self.humidity_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.gas_char is None:
            self.gas_char = self.environment_service.getCharacteristics(self.gas_char_uuid)[0]
            e_gas_handle = self.gas_char.getHandle()
            self.gas_cccd = self.gas_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.color_char is None:
            self.color_char = self.environment_service.getCharacteristics(self.color_char_uuid)[0]
            e_color_handle = self.color_char.getHandle()
            self.color_cccd = self.color_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.config_char is None:
            self.config_char = self.environment_service.getCharacteristics(self.config_char_uuid)[0]

    def set_temperature_notification(self, state):
        if self.temperature_cccd is not None:
            if state == True:
                self.temperature_cccd.write(b"\x01\x00", True)
            else:
                self.temperature_cccd.write(b"\x00\x00", True)

    def set_pressure_notification(self, state):
        if self.pressure_cccd is not None:
            if state == True:
                self.pressure_cccd.write(b"\x01\x00", True)
            else:
                self.pressure_cccd.write(b"\x00\x00", True)

    def set_humidity_notification(self, state):
        if self.humidity_cccd is not None:
            if state == True:
                self.humidity_cccd.write(b"\x01\x00", True)
            else:
                self.humidity_cccd.write(b"\x00\x00", True)

    def set_gas_notification(self, state):
        if self.gas_cccd is not None:
            if state == True:
                self.gas_cccd.write(b"\x01\x00", True)
            else:
                self.gas_cccd.write(b"\x00\x00", True)

    def set_color_notification(self, state):
        if self.color_cccd is not None:
            if state == True:
                self.color_cccd.write(b"\x01\x00", True)
            else:
                self.color_cccd.write(b"\x00\x00", True)

    def configure(self, temp_int=None, press_int=None, humid_int=None, gas_mode_int=None,
                        color_int=None, color_sens_calib=None):
        if temp_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, temp_int, 0)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if press_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, press_int, 1)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if humid_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, humid_int, 2)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if gas_mode_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint8(current_config, gas_mode_int, 8)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if color_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, color_int, 3)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if color_sens_calib is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint8(current_config, color_sens_calib[0], 9)
            new_config = write_uint8(current_config, color_sens_calib[1], 10)
            new_config = write_uint8(current_config, color_sens_calib[2], 11)
            self.config_char.write(binascii.a2b_hex(new_config), True)

    def disable(self):
        set_temperature_notification(False)
        set_pressure_notification(False)
        set_humidity_notification(False)
        set_gas_notification(False)
        set_color_notification(False)


class UserInterfaceService():
    """
    User interface service module. Instance the class and enable to get access to the UI interface.
    """
    serviceUUID = Nordic_UUID(USER_INTERFACE_SERVICE_UUID)
    led_char_uuid = Nordic_UUID(UI_LED_CHAR_UUID)
    btn_char_uuid = Nordic_UUID(UI_BUTTON_CHAR_UUID)
    # To be added: EXT PIN CHAR

    def __init__(self, periph):
        self.periph = periph
        self.ui_service = None
        self.led_char = None
        self.btn_char = None
        self.btn_char_cccd = None
        # To be added: EXT PIN CHAR

    def enable(self):
        """ Enables the class by finding the service and its characteristics. """
        global ui_button_handle

        if self.ui_service is None:
            self.ui_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.led_char is None:
            self.led_char = self.ui_service.getCharacteristics(self.led_char_uuid)[0]
        if self.btn_char is None:
            self.btn_char = self.ui_service.getCharacteristics(self.btn_char_uuid)[0]
            ui_button_handle = self.btn_char.getHandle()
            self.btn_char_cccd = self.btn_char.getDescriptors(forUUID=CCCD_UUID)[0]

    def set_led_mode_off(self):
        self.led_char.write(b"\x00", True)
        
    def set_led_mode_constant(self, r, g, b):
        teptep = "01{:02X}{:02X}{:02X}".format(r, g, b)
        self.led_char.write(binascii.a2b_hex(teptep), True)
        
    def set_led_mode_breathe(self, color, intensity, delay):
        """
        Set LED to breathe mode.
        color has to be within 0x01 and 0x07
        intensity [%] has to be within 1-100
        delay [ms] has to be within 1 ms - 10 s
        """
        teptep = "02{:02X}{:02X}{:02X}{:02X}".format(color, intensity,
                delay & 0xFF, delay >> 8)
        self.led_char.write(binascii.a2b_hex(teptep), True)
        
    def set_led_mode_one_shot(self, color, intensity):  
        """
        Set LED to one shot mode.
        color has to be within 0x01 and 0x07
        intensity [%] has to be within 1-100
        """
        teptep = "03{:02X}{:02X}".format(color, intensity)
        self.led_char.write(binascii.a2b_hex(teptep), True)

    def set_btn_notification(self, state):
        if self.btn_char_cccd is not None:
            if state == True:
                self.btn_char_cccd.write(b"\x01\x00", True)
            else:
                self.btn_char_cccd.write(b"\x00\x00", True)

    def disable(self):
        set_btn_notification(False)


class MotionService():
    """
    Motion service module. Instance the class and enable to get access to the Motion interface.
    """
    serviceUUID =           Nordic_UUID(MOTION_SERVICE_UUID)
    config_char_uuid =      Nordic_UUID(M_CONFIG_CHAR_UUID)
    tap_char_uuid =         Nordic_UUID(M_TAP_CHAR_UUID)
    orient_char_uuid =      Nordic_UUID(M_ORIENTATION_CHAR_UUID)
    quaternion_char_uuid =  Nordic_UUID(M_QUATERNION_CHAR_UUID)
    stepcnt_char_uuid =     Nordic_UUID(M_STEP_COUNTER_UUID)
    rawdata_char_uuid =     Nordic_UUID(M_RAW_DATA_CHAR_UUID)
    euler_char_uuid =       Nordic_UUID(M_EULER_CHAR_UUID)
    rotation_char_uuid =    Nordic_UUID(M_ROTATION_MATRIX_CHAR_UUID)
    heading_char_uuid =     Nordic_UUID(M_HEAIDNG_CHAR_UUID)
    gravity_char_uuid =     Nordic_UUID(M_GRAVITY_VECTOR_CHAR_UUID)

    def __init__(self, periph):
        self.periph = periph
        self.motion_service = None
        self.config_char = None
        self.tap_char = None
        self.tap_char_cccd = None
        self.orient_char = None
        self.orient_cccd = None
        self.quaternion_char = None
        self.quaternion_cccd = None
        self.stepcnt_char = None
        self.stepcnt_cccd = None
        self.rawdata_char = None
        self.rawdata_cccd = None
        self.euler_char = None
        self.euler_cccd = None
        self.rotation_char = None
        self.rotation_cccd = None
        self.heading_char = None
        self.heading_cccd = None
        self.gravity_char = None
        self.gravity_cccd = None

    def enable(self):
        """ Enables the class by finding the service and its characteristics. """
        global m_tap_handle
        global m_orient_handle
        global m_quaternion_handle
        global m_stepcnt_handle
        global m_rawdata_handle
        global m_euler_handle
        global m_rotation_handle
        global m_heading_handle
        global m_gravity_handle

        if self.motion_service is None:
            self.motion_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.config_char is None:
            self.config_char = self.motion_service.getCharacteristics(self.config_char_uuid)[0]
        if self.tap_char is None:
            self.tap_char = self.motion_service.getCharacteristics(self.tap_char_uuid)[0]
            m_tap_handle = self.tap_char.getHandle()
            self.tap_char_cccd = self.tap_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.orient_char is None:
            self.orient_char = self.motion_service.getCharacteristics(self.orient_char_uuid)[0]
            m_orient_handle = self.orient_char.getHandle()
            self.orient_cccd = self.orient_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.quaternion_char is None:
            self.quaternion_char = self.motion_service.getCharacteristics(self.quaternion_char_uuid)[0]
            m_quaternion_handle = self.quaternion_char.getHandle()
            self.quaternion_cccd = self.quaternion_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.stepcnt_char is None:
            self.stepcnt_char = self.motion_service.getCharacteristics(self.stepcnt_char_uuid)[0]
            m_stepcnt_handle = self.stepcnt_char.getHandle()
            self.stepcnt_cccd = self.stepcnt_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.rawdata_char is None:
            self.rawdata_char = self.motion_service.getCharacteristics(self.rawdata_char_uuid)[0]
            m_rawdata_handle = self.rawdata_char.getHandle()
            self.rawdata_cccd = self.rawdata_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.euler_char is None:
            self.euler_char = self.motion_service.getCharacteristics(self.euler_char_uuid)[0]
            m_euler_handle = self.euler_char.getHandle()
            self.euler_cccd = self.euler_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.rotation_char is None:
            self.rotation_char = self.motion_service.getCharacteristics(self.rotation_char_uuid)[0]
            m_rotation_handle = self.rotation_char.getHandle()
            self.rotation_cccd = self.rotation_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.heading_char is None:
            self.heading_char = self.motion_service.getCharacteristics(self.heading_char_uuid)[0]
            m_heading_handle = self.heading_char.getHandle()
            self.heading_cccd = self.heading_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.gravity_char is None:
            self.gravity_char = self.motion_service.getCharacteristics(self.gravity_char_uuid)[0]
            m_gravity_handle = self.gravity_char.getHandle()
            self.gravity_cccd = self.gravity_char.getDescriptors(forUUID=CCCD_UUID)[0]

    def set_tap_notification(self, state):
        if self.tap_char_cccd is not None:
            if state == True:
                self.tap_char_cccd.write(b"\x01\x00", True)
            else:
                self.tap_char_cccd.write(b"\x00\x00", True)

    def set_orient_notification(self, state):
        if self.orient_cccd is not None:
            if state == True:
                self.orient_cccd.write(b"\x01\x00", True)
            else:
                self.orient_cccd.write(b"\x00\x00", True)

    def set_quaternion_notification(self, state):
        if self.quaternion_cccd is not None:
            if state == True:
                self.quaternion_cccd.write(b"\x01\x00", True)
            else:
                self.quaternion_cccd.write(b"\x00\x00", True)

    def set_stepcnt_notification(self, state):
        if self.stepcnt_cccd is not None:
            if state == True:
                self.stepcnt_cccd.write(b"\x01\x00", True)
            else:
                self.stepcnt_cccd.write(b"\x00\x00", True)

    def set_rawdata_notification(self, state):
        if self.rawdata_cccd is not None:
            if state == True:
                self.rawdata_cccd.write(b"\x01\x00", True)
            else:
                self.rawdata_cccd.write(b"\x00\x00", True)

    def set_euler_notification(self, state):
        if self.euler_cccd is not None:
            if state == True:
                self.euler_cccd.write(b"\x01\x00", True)
            else:
                self.euler_cccd.write(b"\x00\x00", True)

    def set_rotation_notification(self, state):
        if self.rotation_cccd is not None:
            if state == True:
                self.rotation_cccd.write(b"\x01\x00", True)
            else:
                self.rotation_cccd.write(b"\x00\x00", True)

    def set_heading_notification(self, state):
        if self.heading_cccd is not None:
            if state == True:
                self.heading_cccd.write(b"\x01\x00", True)
            else:
                self.heading_cccd.write(b"\x00\x00", True)

    def set_gravity_notification(self, state):
        if self.gravity_cccd is not None:
            if state == True:
                self.gravity_cccd.write(b"\x01\x00", True)
            else:
                self.gravity_cccd.write(b"\x00\x00", True)

    def configure(self, step_int=None, temp_comp_int=None, magnet_comp_int=None,
                        motion_freq=None, wake_on_motion=None):
        if step_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, step_int, 0)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if temp_comp_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, temp_comp_int, 1)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if magnet_comp_int is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, magnet_comp_int, 2)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if motion_freq is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint16(current_config, motion_freq, 3)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if wake_on_motion is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint8(current_config, wake_on_motion, 8)
            self.config_char.write(binascii.a2b_hex(new_config), True)

    def disable(self):
        set_tap_notification(False)
        set_orient_notification(False)
        set_quaternion_notification(False)
        set_stepcnt_notification(False)
        set_rawdat_notification(False)
        set_euler_notification(False)
        set_rotation_notification(False)
        set_heading_notification(False)
        set_gravity_notification(False)


class SoundService():
    """
    Sound service module. Instance the class and enable to get access to the Sound interface.
    """
    serviceUUID                 = Nordic_UUID(SOUND_SERVICE_UUID)
    config_char_uuid            = Nordic_UUID(S_CONFIG_CHAR_UUID)
    speaker_data_char_uuid      = Nordic_UUID(S_SPEAKER_DATA_CHAR_UUID)
    speaker_status_char_uuid    = Nordic_UUID(S_SPEAKER_STATUS_CHAR_UUID)
    microphone_char_uuid        = Nordic_UUID(S_MICROPHONE_CHAR_UUID)

    def __init__(self, periph):
        self.periph = periph
        self.sound_service = None
        self.config_char = None
        self.speaker_data_char = None
        self.speaker_status_char = None
        self.speaker_status_char_cccd = None
        self.microphone_char = None
        self.microphone_char_cccd = None

    def enable(self):
        """ Enables the class by finding the service and its characteristics. """
        global s_speaker_status_handle
        global s_microphone_handle

        if self.sound_service is None:
            self.sound_service = self.periph.getServiceByUUID(self.serviceUUID)
        if self.config_char is None:
            self.config_char = self.sound_service.getCharacteristics(self.config_char_uuid)[0]
        if self.speaker_data_char is None:
            self.speaker_data_char = self.sound_service.getCharacteristics(self.speaker_data_char_uuid)[0]
        if self.speaker_status_char is None:
            self.speaker_status_char = self.sound_service.getCharacteristics(self.speaker_status_char_uuid)[0]
            s_speaker_status_handle = self.speaker_status_char.getHandle()
            self.speaker_status_char_cccd = self.speaker_status_char.getDescriptors(forUUID=CCCD_UUID)[0]
        if self.microphone_char is None:
            self.microphone_char = self.sound_service.getCharacteristics(self.microphone_char_uuid)[0]
            s_microphone_handle = self.microphone_char.getHandle()
            self.microphone_char_cccd = self.microphone_char.getDescriptors(forUUID=CCCD_UUID)[0]

    def play_speaker_sample(self, sample=0):
        if self.speaker_data_char is not None:
            sample_str = "{:02X}".format(sample)
            self.speaker_data_char.write(binascii.a2b_hex(sample_str), False)

    def set_speaker_status_notification(self, state):
        if self.speaker_status_char_cccd is not None:
            if state == True:
                self.speaker_status_char_cccd.write(b"\x01\x00", True)
            else:
                self.speaker_status_char_cccd.write(b"\x00\x00", True)

    def set_microphone_notification(self, state):
        if self.microphone_char_cccd is not None:
            if state == True:
                self.microphone_char_cccd.write(b"\x01\x00", True)
            else:
                self.microphone_char_cccd.write(b"\x00\x00", True)

    def configure(self, speaker_mode=None, microphone_mode=None):
        if speaker_mode is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint8(current_config, speaker_mode, 0)
            self.config_char.write(binascii.a2b_hex(new_config), True)
        if microphone_mode is not None and self.config_char is not None:
            current_config = binascii.b2a_hex(self.config_char.read())
            new_config = write_uint8(current_config, microphone_mode, 1)
            self.config_char.write(binascii.a2b_hex(new_config), True)

    def disable(self):
        set_speaker_status_notification(False)
        set_microphone_notification(False)

class MyDelegate(DefaultDelegate):

    def handleNotification(self, hnd, data):
        #Debug print repr(data)
        timestamp=datetime.datetime.now()
        if (hnd == e_temperature_handle):
            teptep = binascii.b2a_hex(data)

            #temperature_str='{}.{}'.format(self._str_to_int(teptep[:-2]), int(teptep[-2:], 16)))
            temperature_str=str(self._str_to_int(teptep[:-2]))+'.'+str(int(teptep[-2:], 16))

            notification_value={"value":temperature_str, "description":"degree celsius"}
            send_telemetry(ENVIRONMENT_SERVICE_TOPIC, TEMPERATURE_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Temp received:  {}.{} degCelcius'.format(
                        self._str_to_int(teptep[:-2]), int(teptep[-2:], 16)))

        elif (hnd == e_pressure_handle):
            pressure_int, pressure_dec = self._extract_pressure_data(data)

            #pressure_str='{}.{}'.format(self._str_to_int(teptep[:-2]), int(teptep[-2:], 16)))
            pressure_str=str(pressure_int)+'.'+str(pressure_dec)

            notification_value={"value":pressure_str, "description":"hPa"}
            send_telemetry(ENVIRONMENT_SERVICE_TOPIC, PRESSURE_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Press received: {}.{} hPa'.format(
                        pressure_int, pressure_dec))

        elif (hnd == e_humidity_handle):
            teptep = binascii.b2a_hex(data)

            humidity_str=str(self._str_to_int(teptep))
            notification_value={"value":humidity_str, "description":"%"}
            send_telemetry(ENVIRONMENT_SERVICE_TOPIC, HUMIDITY_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Humidity received: {} %'.format(self._str_to_int(teptep)))

        elif (hnd == e_gas_handle):
            eco2, tvoc = self._extract_gas_data(data)

            eco2_str=str(eco2)
            tvoc_str=str(tvoc)
            notification_value={"value_1":eco2_str, "description_1":"eCO2 ppm","value_2":tvoc_str, "description_2":"TVOC ppb"}
            send_telemetry(HUMIDITY_CHARACTERISTIC_TOPIC,GAS_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Gas received: eCO2 ppm: {}, TVOC ppb: {} %'.format(eco2, tvoc))

        elif (hnd == e_color_handle):
            teptep = binascii.b2a_hex(data)

            color_str=str(teptep)
            color_str=color_str[2:-1]
            notification_value={"value":color_str, "description":"RGBW value"}
            send_telemetry(HUMIDITY_CHARACTERISTIC_TOPIC,COLOR_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Color: {}'.format(teptep))

        elif (hnd == ui_button_handle):
            teptep = binascii.b2a_hex(data)

            button_str=str(int.from_bytes(data,byteorder="big",signed=False))
            notification_value={"value":button_str, "description":"1->pressed, 0->released"}
            send_telemetry(UI_SERVICE_TOPIC,BUTTON_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Button state [1 -> released]: {}'.format(self._str_to_int(teptep)))

        elif (hnd == m_tap_handle):
            direction, count = self._extract_tap_data(data)

            tapdir_str=str(int.from_bytes(data[0:1],byteorder="big",signed=False))
            tapcount_str=str(int.from_bytes(data[1:2],byteorder="big",signed=False))
            notification_value={"value_1":tapdir_str, "description_1":"direction","value_2":tapcount_str, "description_2":"count"}
            send_telemetry(MOTION_SERVICE_TOPIC,TAP_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Tap: direction: {}, count: {}'.format(direction, self._str_to_int(count)))

        elif (hnd == m_orient_handle):

            orientation_str=str(int.from_bytes(data,byteorder="big",signed=False))

            notification_value={"value":orientation_str, "description":"orientation"}
            send_telemetry(MOTION_SERVICE_TOPIC,ORIENTATION_CHARACTERISTIC_TOPIC,notification_value)

            teptep = binascii.b2a_hex(data)
            print('[THINGY - BLE] Notification: Orient: {}'.format(teptep))

        elif (hnd == m_quaternion_handle):
            teptep = binascii.b2a_hex(data)

            quaternion_str=str(teptep)
            quaternion_str=quaternion_str[2:-1]
            notification_value={"value":quaternion_str, "description":"quaternion"}
            send_telemetry(MOTION_SERVICE_TOPIC,QUATERNION_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Quaternion: {}'.format(teptep))

        elif (hnd == m_stepcnt_handle):
            teptep = binascii.b2a_hex(data)

            stepcount_str=str(int.from_bytes(data[0:4],byteorder="little",signed=False))
            time_str=str(int.from_bytes(data[4:7],byteorder="little",signed=False))

            notification_value={"value_1":stepcount_str, "description_1":"Step Count","value_2":time_str, "description":"time [ms]"}
            send_telemetry(MOTION_SERVICE_TOPIC,STEPCOUNTER_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Step Count: {}'.format(teptep))

        elif (hnd == m_rawdata_handle):
            teptep = binascii.b2a_hex(data)

            data_=[]
            #print("DEBUG - data len: "+str(len(data)))
            for i in range(int(len(data)/2)):
                if i < 3: #acceleration data 6Q10 fixed point
                    data_.append(float(int.from_bytes(data[i*2:(i*2)+2],byteorder='little', signed=True))/(2**10))
                elif i < 6: #gyroscope data 11Q5 fixed point
                    data_.append(float(int.from_bytes(data[i*2:(i*2)+2],byteorder='little', signed=True))/(2**5))
                else:    #compass data 12Q4 fixed point
                    data_.append(float(int.from_bytes(data[i*2:(i*2)+2],byteorder='little', signed=True))/(2**4))
            #print('DEBUG - data: '+str(data_))

            rawdata_str=str(teptep)
            rawdata_str=rawdata_str[2:-1]
            notification_value={"acc":data_[0:3],"gyro":data_[3:6],"compass":data_[6:9]} #acc i [G], gyro in [deg/s], compass [uT]
            send_telemetry(MOTION_SERVICE_TOPIC,RAWDATA_CHARACTERISTIC_TOPIC,notification_value,timestamp)

            print('[THINGY - BLE] Notification: Raw data: {}'.format(teptep))

        elif (hnd == m_euler_handle):
            teptep = binascii.b2a_hex(data)

            euler_str=str(teptep)
            euler_str=euler_str[2:-1]
            notification_value={"value":euler_str, "description":"euler"}
            send_telemetry(MOTION_SERVICE_TOPIC,EULER_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Euler: {}'.format(teptep))

        elif (hnd == m_rotation_handle):
            teptep = binascii.b2a_hex(data)

            rotation_str=str(teptep)
            rotation_str=rotation_str[2:-1]
            notification_value={"value":rotation_str, "description":"rotation matrix"}
            send_telemetry(MOTION_SERVICE_TOPIC,ROTATION_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Rotation matrix: {}'.format(teptep))

        elif (hnd == m_heading_handle):
            teptep = binascii.b2a_hex(data)

            heading_str=str(int.from_bytes(data[2:4],byteorder="little",signed=True))+'.'+str(int.from_bytes(data[0:2],byteorder="little",signed=False))
            notification_value={"value":heading_str, "description":"degrees"}
            send_telemetry(MOTION_SERVICE_TOPIC,HEADING_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Heading: {}'.format(teptep))

        elif (hnd == m_gravity_handle):
            teptep = binascii.b2a_hex(data)

            gravity_str=str(teptep)
            gravity_str=gravity_str[2:-1]
            notification_value={"value":gravity_str, "description":"gravity vector"}
            send_telemetry(MOTION_SERVICE_TOPIC,GRAVITY_CHARACTERISTIC_TOPIC,notification_value)

            print('[THINGY - BLE] Notification: Gravity: {}'.format(teptep))

        elif (hnd == s_speaker_status_handle):
            teptep = binascii.b2a_hex(data)

            speaker_status_str=str(teptep)
            speaker_status_str=speaker_status_str[2:-1]
            notification_value={"value":speaker_status_str, "description":"speaker status"}
            send_telemetry(SOUND_SERVICE_TOPIC,SPEAKER_CHARACTERISTIC_TOPIC,notification_value)
            print('[THINGY - BLE] Notification: Speaker Status: {}'.format(teptep))

        elif (hnd == s_microphone_handle):
            teptep = binascii.b2a_hex(data)
            print('[THINGY - BLE] Notification: Microphone: {}'.format(teptep))
            
            print('[THINGY - BLE] WARNING, NOT IMPLEMENTED. Micrphone data not forwarded to MQTT for now.')
        else:
            teptep = binascii.b2a_hex(data)
            print('[THINGY - BLE] Notification: UNKOWN: hnd {}, data {}'.format(hnd, teptep))
            

    def _str_to_int(self, s):
        """ Transform hex str into int. """
        i = int(s, 16)
        if i >= 2**7:
            i -= 2**8
        return i

    def _extract_pressure_data(self, data):
        """ Extract pressure data from data string. """
        teptep = binascii.b2a_hex(data)
        pressure_int = 0
        for i in range(0, 4):
                pressure_int += (int(teptep[i*2:(i*2)+2], 16) << 8*i)
        pressure_dec = int(teptep[-2:], 16)
        return (pressure_int, pressure_dec)

    def _extract_gas_data(self, data):
        """ Extract gas data from data string. """
        teptep = binascii.b2a_hex(data)
        eco2 = int(teptep[:2],16) + (int(teptep[2:4],16) << 8)
        tvoc = int(teptep[4:6],16) + (int(teptep[6:8],16) << 8)
        return eco2, tvoc

    def _extract_tap_data(self, data):
        """ Extract tap data from data string. """
        teptep = binascii.b2a_hex(data)
        direction = teptep[0:2]
        count = teptep[2:4]
        return (direction, count)


class Thingy52(Peripheral):
    """
    Thingy:52 module. Instance the class and enable to get access to the Thingy:52 Sensors.
    The addr of your device has to be know, or can be found by using the hcitool command line 
    tool, for example. Call "> sudo hcitool lescan" and your Thingy's address should show up.
    """
    def __init__(self, addr):
        Peripheral.__init__(self, addr, addrType=ADDR_TYPE_RANDOM)

        # Thingy configuration service not implemented
        self.battery = BatterySensor(self)
        self.environment = EnvironmentService(self)
        self.ui = UserInterfaceService(self)
        self.motion = MotionService(self)
        self.sound = SoundService(self)
        # DFU Service not implemented

################################################  MQTT CALLBACKS  #############################################

def on_publish_cb(client, data, result):
    print("[THINGY - MQTT] on_publish. data: "+ str(data) +", result: "+str(result))
    pass

def on_disconnect_cb(client, userdata, result):
    global mqtt_connection_active
    print("[MQTT] on_disconnect_cb.")
    mqtt_connection_active=False
    pass

# The callback for when the client receives a CONNACK response from the server.
def on_connect_cb(client, userdata, flags, rc):
    global mqtt_connection_active

    if rc==0:
        mqtt_connection_active=True #TODO: This doesn't change value, maybe threads problem
    else:
        mqtt_connection_active=False

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.  

    subscribe_topic=RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC+"/#"
    print("[THINGY - MQTT] on connect result code "+str(rc)+". Subscribing to topic: "+subscribe_topic)
    client.subscribe(subscribe_topic)

def on_message_cb(client, userdata, msg):
    global mqtt_incoming_message_queue
    try:
        dec_message=msg.payload.decode("utf-8")
        
        entry={"payload":dec_message, "topic":msg.topic}
        mqtt_incoming_message_queue.append(entry) #use a queue instead of directly calling message_handler because bluepy is not threadsafe
        #message_handler(entry["payload"],entry["topic"])
    except Exception as e:
        print("[THINGY - MQTT] Exception in on_message!: "+ str(e))
        traceback.print_exc()

################################################  MQTT HELPERS  #############################################

def connect_mqtt():

    print("[THINGY - MQTT] Connecting...")
    m_client=mqtt.Client()
    m_client.on_publish=on_publish_cb
    m_client.on_connect=on_connect_cb
    m_client.on_message=on_message_cb
    m_client.on_disconnect=on_disconnect_cb

    m_client.username_pw_set(device_token, device_password)
    m_client.connect(mqtt_broker, mqtt_port)
    return m_client

def publish_mqtt(mqtt_client,data_to_publish,topic,timestamp=None):
    global mqtt_connection_active

    qos=1
    try:
        msg_uuid=str(uuid.uuid4())
        data_to_publish['messageID']=msg_uuid
        if timestamp==None:
            timestamp=datetime.datetime.now()
        ts=unix_time_millis(timestamp)
        data_to_publish['tsUnixTime']=ts

        print("[THINGY - MQTT] Publishing this data: "+json.dumps(data_to_publish)+" on topic: "+topic)
        ret=mqtt_client.publish(topic,json.dumps(data_to_publish),qos)
        print("[THINGY - MQTT] Publish return code: "+str(ret.rc))
    except Exception as e:
        print("[THINGY - MQTT] Exception in publish_mqtt!: "+ str(e))

def send_rpc_response(append_topic, method, responseID, return_value):
    topic=RPC_RESPONSE_TOPIC+'/'+THINGY_TOPIC+'/'+append_topic
    
    response_msg={'method':method , 'inReplyTo':responseID, 'params':{'return':return_value}, 'path':append_topic}
    publish_mqtt(mqtt_client,response_msg,topic)

def send_telemetry(sensorClass,sensorType,reading,timestamp=None):
    global mqtt_client
    global DEVICE_ID
    global THINGY_TOPIC

    topic=TELEMETRY_TOPIC+'/'+THINGY_TOPIC
    if sensorClass != None:
        topic=topic+'/'+sensorClass
    else:
        sensorClass=""

    if sensorType != None:
        topic=topic+'/'+sensorType
    else:
        sensorType=""

    #telemetry_uuid=str(uuid.uuid4())

    msg={"sensorClass":sensorClass, "sensorType":sensorType, "reading":reading, "sourceName":DEVICE_READABLE_NAME, "sourceUID":DEVICE_ID}
    
    publish_mqtt(mqtt_client,msg,topic,timestamp)

def message_handler(msg,topic):
    global thingy

    try:
        jobj=json.loads(msg)
        
        if len(str(jobj))<300:
            print("[THINGY - MQTT] message received with topic: "+topic+" msg: "+str(jobj))
        else:
            print("[THINGY - MQTT] message received with topic: "+topic)

        topic_parts=topic.split('/')
        #TODO Filter based on THINGY topic and reject other requests
        if (topic_parts[0]+'/'+topic_parts[1]+'/'+topic_parts[2]==RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC): #the message is for me
            target_service='/'
            target_characteristic='/'

            responseTopic=""

            if len(topic_parts)>=4:
                target_service=topic_parts[3]
                responseTopic=target_service

            if len(topic_parts)>=5:
                target_characteristic=topic_parts[4]
                responseTopic=responseTopic+'/'+target_characteristic


            messageID=jobj["messageID"]
            request_method=jobj["method"]
            request_params=None
            try:
                request_params=jobj["params"]
            except KeyError as e:
                print("[THINGY - MQTT] WARNING, no params found in the message.")
                pass

            ret = processHumanReadConfigRequest(thingy,target_service,target_characteristic,request_method,request_params,messageID)
            
            print("[THINGY - DEBUG] MQTT message processed.")

            send_rpc_response(responseTopic,request_method,messageID,ret)
        else:
            print("[THINGY - MQTT] The message is not for me..."+topic_parts[0]+'/'+topic_parts[1]+'/'+topic_parts[2]+" != " +RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC)
            #for p in topic_parts:
            #    print("[MQTT] "+p)
            pass

    except Exception as e:
        #with print_lock:
        print("[THINGY - MQTT] Exception in multireader_message_handler!: "+ str(e))
        traceback.print_exc()
        pass

################################################  MQTT RPC HANDLERS  #############################################

def processHumanReadConfigRequest(ble_device,target_service,target_characteristic,request_method,request_params,messageID):

    if(len(str(request_params))<300):
        print("[THINGY - RPC] processHumanReadConfigRequest - target_service: "+target_service+" target_characteristic: "+target_characteristic+" request_method: "+request_method+" request_params:"+str(request_params))
    else:
        print("[THINGY - RPC] processHumanReadConfigRequest - target_service: "+target_service+" target_characteristic: "+target_characteristic+" request_method: "+request_method)
   
    if target_service==ENVIRONMENT_SERVICE_TOPIC:
        return processEnvConfigReq(ble_device,target_characteristic,request_method,request_params,messageID)
    elif target_service==UI_SERVICE_TOPIC:
        return processUiConfigReq(ble_device,target_characteristic,request_method,request_params,messageID)
    elif target_service==MOTION_SERVICE_TOPIC:
        return processMotionConfigReq(ble_device,target_characteristic,request_method,request_params,messageID)
    elif target_service==SOUND_SERVICE_TOPIC:
        return processSoundConfigReq(ble_device,target_characteristic,request_method,request_params,messageID)
    else:
        print("[THINGY - RPC] Config of service: "+str(target_service)+" not available!")
        return UNKNOWN_SERVICE

def processEnvConfigReq(ble_device,target_characteristic,request_method,request_params,messageID):

    if target_characteristic==TEMPERATURE_CHARACTERISTIC_TOPIC:
        if request_method == 'interval':
            t_int=request_params
            print("[THINGY - RPC] Setting temperature interval to: "+str(t_int)+" ms")
            if t_int:
                ble_device.environment.enable()
                ble_device.environment.configure(temp_int=t_int)
                ble_device.environment.set_temperature_notification(True)
            else:
                ble_device.environment.set_temperature_notification(False)

            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==PRESSURE_CHARACTERISTIC_TOPIC:
        if request_method == 'interval':
            p_int=request_params
            #print("[APPLICATION] Setting pressure interval to: "+str(p_int)+" ms")
            if p_int:
                ble_device.environment.enable()
                ble_device.environment.configure(press_int=p_int)
                ble_device.environment.set_pressure_notification(True)
            else:
                ble_device.environment.set_pressure_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==HUMIDITY_CHARACTERISTIC_TOPIC:
        if request_method == 'interval':
            h_int=request_params
            #print("[APPLICATION] Setting humidity interval to: "+str(h_int)+" ms")
            if h_int:
                ble_device.environment.enable()
                ble_device.environment.configure(humid_int=p_int)
                ble_device.environment.set_humidity_notification(True)
            else:
                ble_device.environment.set_humidity_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==GAS_CHARACTERISTIC_TOPIC:
        if request_method == 'mode':
            g_mode=request_params
            if g_mode == 1 or g_mode == 2 or g_mode == 3: #1: 1s interval, 2: 10s interval, 3: 60s interval
                ble_device.environment.enable()
                ble_device.environment.configure(gas_mode_int=g_mode)
                ble_device.environment.set_gas_notification(True)
                return SUCCESS
            elif g_mode == 0:
                ble_device.environment.set_gas_notification(False)
                return SUCCESS
            else:
                print("[THINGY - RPC] Invalid gas mode. Valid ones are: 1->1s interval, 2->10s interval, 3->60s interval")
                return VALUE_ERROR
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==COLOR_CHARACTERISTIC_TOPIC:
        if request_method == 'interval':
            c_int=request_params
            if c_int:
                ble_device.environment.enable()
                ble_device.environment.configure(color_int=c_int)
                ble_device.environment.set_color_notification(True)
            else:
                ble_device.environment.set_color_notification(False)
            return SUCCESS
        elif request_method=='calibration':
            c_calib=request_params
            if len(c_calib)==3:
                ble_device.environment.enable()
                ble_device.environment.configure(color_sens_calib=c_calib)
                return SUCCESS
            else:
                print("[THINGY - RPC] Invalid color calibration. It must be an array of three uint8_t, see Thingy documentation.")
                return VALUE_ERROR
        else:
            return UNKNOWN_METHOD

    else:
        print("[THINGY - RPC] Unknown characteristic: "+target_characteristic+" for service: "+ENVIRONMENT_SERVICE_TOPIC)
        return UNKNOWN_CHARACTERISTIC     

def processUiConfigReq(ble_device,target_characteristic,request_method,request_params,messageID):

    if target_characteristic==BUTTON_CHARACTERISTIC_TOPIC:
        if request_method == 'enable':
            k_enable=request_params
            ble_device.ui.enable()
            ble_device.ui.set_btn_notification(k_enable!=0)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==LED_CHARACTERISTIC_TOPIC:
        if request_method == 'set-led':
            led_mode=request_params['mode']
            ble_device.ui.enable()
            if led_mode=='off':
                ble_device.ui.set_led_mode_off()
                return SUCCESS
            elif led_mode=='constant':
                R=request_params['R']
                G=request_params['G']
                B=request_params['B']
                ble_device.ui.set_led_mode_constant(R,G,B)
                return SUCCESS
            elif led_mode=='breathe':
                color=request_params['color']
                intensity=request_params['intensity']
                delay=request_params['delay']
                ble_device.ui.set_led_mode_breathe(color,intensity,delay) #set_led_mode_breathe(0x01, 50, 100)
                return SUCCESS
            elif led_mode=='one-shot':
                color=request_params['color']
                intensity=request_params['intensity']
                ble_device.ui.set_led_mode_one_shot(color,intensity)
                return SUCCESS
            else:
                print("[THINGY - RPC] Unknown led mode.")
                return VALUE_ERROR
        elif request_method == 'play-animation':
            request_params["messageID"]=messageID
            led_animation_request(request_params)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    else:
        print("[THINGY - RPC] Unknown characteristic: "+target_characteristic+" for service: "+ENVIRONMENT_SERVICE_TOPIC)
        return UNKNOWN_CHARACTERISTIC

def processMotionConfigReq(ble_device,target_characteristic,request_method,request_params,messageID):

    if target_characteristic=="/": #empty characteristic means that the configuration is service wide
        if request_method == 'temp_comp_interval':
            t_c_int=request_params
            if s_int:
                ble_device.motion.enable()
                ble_device.motion.configure(temp_comp_int=t_c_int)
                return SUCCESS
            else:
                return VALUE_ERROR
        elif request_method=='magnet_comp_int':
            m_c_int=request_params
            if s_int:
                ble_device.motion.enable()
                ble_device.motion.configure(magnet_comp_int=m_c_int)
                return SUCCESS
            else:
                return VALUE_ERROR
        elif request_method=='motion_freq':
            m_freq=request_params
            if m_freq:
                ble_device.motion.enable()
                ble_device.motion.configure(motion_freq=m_freq)
                return SUCCESS
            else:
                return VALUE_ERROR
        elif request_method=='wake_on_motion':
            w_o_m_conf=humanread_value['wake_on_motion']
            ble_device.motion.enable()
            ble_device.motion.configure(wake_on_motion=w_o_m_conf)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==TAP_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            tap_en=request_params
            if tap_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_tap_notification(True)
            else:
                ble_device.motion.set_tap_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==ORIENTATION_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            orientation_en=request_params
            if orientation_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_orient_notification(True)
            else:
                ble_device.motion.set_orient_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==QUATERNION_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            quaternion_en=request_params
            if quaternion_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_quaternion_notification(True)
            else:
                ble_device.motion.set_quaternion_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==STEPCOUNTER_CHARACTERISTIC_TOPIC:
        if request_method=='interval':
            s_int=request_params
            if s_int:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ, step_int=s_int)
                ble_device.motion.set_stepcnt_notification(True)
            else:
                ble_device.motion.set_stepcnt_notification(False)
            return SUCCESS
        elif request_method=='enable':
            step_en=request_params #this is redundant since there is already the interval setting that enables the step counter.
            if step_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_stepcnt_notification(True)
            else:
                ble_device.motion.set_stepcnt_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==RAWDATA_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            rawdata_en=request_params
            if rawdata_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_rawdata_notification(True)
            else:
                ble_device.motion.set_rawdata_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==EULER_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            euler_en=request_params
            if euler_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_euler_notification(True)
            else:
                ble_device.motion.set_euler_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==ROTATION_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            rotation_en=request_params
            if rotation_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_rotation_notification(True)
            else:
                ble_device.motion.set_rotation_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==HEADING_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            heading_en=request_params
            if heading_en:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_heading_notification(True)
            else:
                ble_device.motion.set_heading_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD

    elif target_characteristic==GRAVITY_CHARACTERISTIC_TOPIC:
        if request_method=='enable':
            gravity_enable=request_params
            if gravity_enable:
                ble_device.motion.enable()
                #ble_device.motion.configure(motion_freq=DEFAULT_MOTION_FREQ)
                ble_device.motion.set_gravity_notification(True)
            else:
                ble_device.motion.set_gravity_notification(False)
            return SUCCESS
        else:
            return UNKNOWN_METHOD
    else:
        print("[THNIGY - RPC] Unknown characteristic: "+target_characteristic+" for service: "+ENVIRONMENT_SERVICE_TOPIC)
        return UNKNOWN_CHARACTERISTIC

def processSoundConfigReq(target_characteristic,request_method,request_params,messageID):

    if target_characteristic==SPEAKER_CHARACTERISTIC_TOPIC:
        if request_method=='mode':
            s_mode=request_params
            if s_mode == 3 :
                ble_device.sound.enable()
                ble_device.sound.configure(speaker_mode=s_mode)
                ble_device.sound.set_speaker_status_notification(True)
                return SUCCESS
            elif s_mode == 0 :
                ble_device.sound.set_speaker_status_notification(False)
                return SUCCESS
            else:
                print("[THINGY - RPC] Only speaker mode=3 is implemented. speaker mode=0 turns off the speaker status notification.")
                return VALUE_ERROR
        
        elif request_method=='enable':
            s_enable=request_params
            if s_enable:
                ble_device.sound.enable()
                ble_device.sound.set_speaker_status_notification(True)
            else:
                ble_device.sound.set_speaker_status_notification(False)
            return SUCCESS

        elif request_method=='data':
            s_data=request_params
            if len(s_data)==1:
                ble_device.sound.play_speaker_sample(s_data)
                return SUCCESS
            else:
                print("[THNIGY - RPC] speaker_data must contain a value in the range [0:8] which is the index of the audio sample to play. Other speaker modes not implemented yes.")
                return VALUE_ERROR

        else:
            return UNKNOWN_METHOD

    else:
        print("[THINGY - RPC] Unknown characteristic: "+target_characteristic+" for service: "+ENVIRONMENT_SERVICE_TOPIC)
        return UNKNOWN_CHARACTERISTIC

################################################  LED ANIMATION  #############################################

def led_animation_request(params):
    global led_animation_queue
    led_animation_queue.append(params)
    return SUCCESS #cannot return anything meaninful here. Audio process happen on a separated thread. The thread will return some meaninful thing once played

class LED_ANIMATION_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def run(self):
        global led_animation_queue
        global thingy
        global syncronous_ble_operation_queue
        DIM_PERCENT=100
        
        while self.__loop:
            try:
                running_animation_request=led_animation_queue.popleft()
                print("[THINGY - LED ANIMATION] new animation pop from the queue. There are "+str(len(led_animation_queue))+" remaining animation in the queue")
                frames=running_animation_request['frames']
                frame_interval=running_animation_request['frameInterval']
                #append=running_animation_request['append']
                repeatTimes=running_animation_request['repeat']
                frameIdx=0
                init_time=time.time()
                animation_len=len(frames)
                animation_uuid=running_animation_request['messageID']
                interrupted_animation_flag=0
                if repeatTimes>0:
                    print("[THINGY - LED ANIMATION] The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps and it will be repeated "+str(repeatTimes)+" times. In total it will last: "+str(animation_len*frame_interval*repeatTimes)+"s")
                else:
                    print("[THINGY - LED ANIMATION] The animation contains "+str(animation_len)+" frames reproduced at "+str(1/frame_interval)+"fps. It will be repeated till a new animation is received (repeat=-1)")
               
                while frameIdx<(animation_len*repeatTimes) or repeatTimes==-1:
                    try: #TODO: this check should be done at the multireader_message_handler, otherwise if two appends are done in a short time it might loose an append=False condition
                        #if animation_that_overwrites!=None:
                        new_animation=led_animation_queue[-1] #do not pop the last animation, just check for the append field.
                        if not new_animation['append'] or repeatTimes==-1: #if the new animation has to overwrite the running one then stop it, the new one will be pop$
                            repeatTimes=0                 #this will stop the animation at the next iteration
                            led_animation_queue.clear()
                            led_animation_queue.append(new_animation) #leave only the last animation in the queue
                            interrupted_animation_flag=1
                    except IndexError as e:
                        pass

                    frame=frames[frameIdx%animation_len]["leds"]
                    FRAME_LEN=len(frame)
                    if FRAME_LEN!=1:
                        print("[THINGY - LED ANIMATION] The frame contains "+str(FRAME_LEN)+" pixels, but thingy supports only one pixel per frame!")

                    if FRAME_LEN>=1:
                        ledIdx = 0
                        pixel=frame[ledIdx]
                        r=int(pixel['r']*DIM_PERCENT/100)
                        g=int(pixel['g']*DIM_PERCENT/100)
                        b=int(pixel['b']*DIM_PERCENT/100)
                        if thingy!=None:
                            #print("[DEBUG] setting r: "+str(r)+" g: "+str(g)+" b: "+str(b))
                            entry={"data":{"r":r,"g":g,"b":b},"method":"set_led_mode_constant"}
                            syncronous_ble_operation_queue.append(entry)
                        else:
                            print("[DEBUG] thingy unavailable")
                            repeatTimes=0                 #this will stop the animation at the next iteration
                            interrupted_animation_flag==2

                    frameIdx=frameIdx+1

                    ex_time=time.time()-init_time
                    sleep_interval=frame_interval-ex_time
                    if sleep_interval<0.001: #sleep at least this time to avoid taking 100% of cpu
                        print("[THINGY - LED ANIMATION] Warning: the time to draw the frame ("+str(ex_time*1000)+"ms)  is longer than the requested interframe interval!")
                        sleep_interval=0.001
                    #print("frameidx: "+str(frameIdx)+" took: "+str(ex_time*1000)+"ms")
                    time.sleep(sleep_interval)
                    init_time=time.time()

                if interrupted_animation_flag==1:
                    ret="interrupted"
                elif interrupted_animation_flag==2:
                    ret="thingy unavailable"
                else:
                    ret="executed"

                request_method="play-animation"
                responseTopic=UI_SERVICE_TOPIC+'/'+LED_CHARACTERISTIC_TOPIC
                send_rpc_response(responseTopic,request_method,running_animation_request['messageID'],ret)

                running_animation_request=None
            except IndexError as e:
                time.sleep(0.1)
                pass


################################################  APPLICATION  #############################################

def main():
    global mqtt_client
    global THINGY_TOPIC
    global DEVICE_ID
    global thingy
    global mqtt_incoming_message_queue
    global DEVICE_READABLE_NAME
    global led_animation_queue
    global syncronous_ble_operation_queue

    parser = argparse.ArgumentParser()
    parser.add_argument('--address', action='store', help='BD (MAC) address of BLE peripheral')
    parser.add_argument('--name', action='store', default="Not_assigned", help='Friendly name for mqtt telemetry')
    parser.add_argument('-n', action='store', dest='count', default=0,type=int, help="Number of times to loop data")
    parser.add_argument('--temperature', action="store_true",default=False)
    parser.add_argument('--pressure', action="store_true",default=False)
    parser.add_argument('--humidity', action="store_true",default=False)
    parser.add_argument('--gas', action="store_true",default=False)
    parser.add_argument('--color', action="store_true",default=False)
    parser.add_argument('--keypress', action='store_true', default=False)
    parser.add_argument('--tap', action='store_true', default=False)
    parser.add_argument('--orientation', action='store_true', default=False)
    parser.add_argument('--quaternion', action='store_true', default=False)
    parser.add_argument('--stepcnt', action='store_true', default=False)
    parser.add_argument('--rawdata', action='store_true', default=False)
    parser.add_argument('--euler', action='store_true', default=False)
    parser.add_argument('--rotation', action='store_true', default=False)
    parser.add_argument('--heading', action='store_true', default=False)
    parser.add_argument('--gravity', action='store_true', default=False)
    #parser.add_argument('--battery', action='store_true', default=False) #battery enbaled by default
    parser.add_argument('--speaker', action='store_true', default=False)
    parser.add_argument('--microphone', action='store_true', default=False)
    args = parser.parse_args()

    mqtt_incoming_message_queue=deque()

    DEVICE_ID=args.address.lower()
    THINGY_TOPIC=DEVICE_ID+"-thingy"
    DEVICE_READABLE_NAME=args.name

    mqtt_client=None
    while mqtt_client==None:
        try:
            mqtt_client=connect_mqtt()
        except Exception as e:
            print("[THINGY - APPLICATION] Exception during mqtt connect. " + str(e))
            time.sleep(1)

    try:
        mqtt_client.loop_start()
    except Exception as e:
        print("[THINGY - APPLICATION] Exception during mqtt loop start. " + str(e))

    led_animation_queue=deque()
    syncronous_ble_operation_queue=deque()
    led_animation_thread=LED_ANIMATION_THREAD(1,"led animation thread")
    led_animation_thread.setDaemon(True)
    led_animation_thread.start()

    thingy=None
    try:
        loop=True
        while loop:
            try:
                print('[THINGY - APPLICATION] Connecting to ' + args.address)
                thingy = Thingy52(args.address)
                print('[THINGY - APPLICATION] Thnigy connected...')
                

                message={"connected":True}
                send_telemetry(None,None,message)

                thingy.setDelegate(MyDelegate())

                # Set LED so that we know we are connected
                thingy.ui.enable()
                #thingy.ui.set_led_mode_breathe(0x01, 50, 100) # 0x01 = RED
                #print('[THINGY - APPLICATION] LED set to breathe mode...')
                thingy.ui.set_led_mode_off()
               
                # Enabling selected sensors
                print('[THINGY - APPLICATION] Enabling battery sensor.')
                thingy.battery.enable()
                # Environment Service
                if args.temperature:
                    thingy.environment.enable()
                    thingy.environment.configure(temp_int=1000)
                    thingy.environment.set_temperature_notification(True)
                if args.pressure:
                    thingy.environment.enable()
                    thingy.environment.configure(press_int=1000)
                    thingy.environment.set_pressure_notification(True)
                if args.humidity:
                    thingy.environment.enable()
                    thingy.environment.configure(humid_int=1000)
                    thingy.environment.set_humidity_notification(True)
                if args.gas:
                    thingy.environment.enable()
                    thingy.environment.configure(gas_mode_int=1)
                    thingy.environment.set_gas_notification(True)
                if args.color:
                    thingy.environment.enable()
                    thingy.environment.configure(color_int=1000)
                    thingy.environment.configure(color_sens_calib=[0,0,0])
                    thingy.environment.set_color_notification(True)
                # User Interface Service
                if args.keypress:
                    thingy.ui.enable()
                    thingy.ui.set_btn_notification(True)
                #if args.battery: #battery enbaled by default
                #    thingy.battery.enable()
                # Motion Service
                if args.tap:
                    thingy.motion.enable()
                    thingy.motion.configure(motion_freq=200)
                    thingy.motion.set_tap_notification(True)
                if args.orientation:
                    thingy.motion.enable()
                    thingy.motion.set_orient_notification(True)
                if args.quaternion:
                    thingy.motion.enable()
                    thingy.motion.set_quaternion_notification(True)
                if args.stepcnt:
                    thingy.motion.enable()
                    thingy.motion.configure(step_int=100)
                    thingy.motion.set_stepcnt_notification(True)
                if args.rawdata:
                    thingy.motion.enable()
                    thingy.motion.set_rawdata_notification(True)
                if args.euler:
                    thingy.motion.enable()
                    thingy.motion.set_euler_notification(True)
                if args.rotation:
                    thingy.motion.enable()
                    thingy.motion.set_rotation_notification(True)
                if args.heading:
                    thingy.motion.enable()
                    thingy.motion.set_heading_notification(True)
                if args.gravity:
                    thingy.motion.enable()
                    thingy.motion.set_gravity_notification(True)
                # Sound Service
                if args.speaker:
                    thingy.sound.enable()
                    thingy.sound.configure(speaker_mode=0x03)
                    thingy.sound.set_speaker_status_notification(True)
                    # Test speaker
                    thingy.sound.play_speaker_sample(1)
                if args.microphone:
                    thingy.sound.enable()
                    thingy.sound.configure(microphone_mode=0x01)
                    thingy.sound.set_microphone_notification(True)

                # Allow sensors time to start up (might need more time for some sensors to be ready)
                print('[THINGY - APPLICATION] All requested sensors and notifications are enabled...')
                time.sleep(0.2)
                
                message={"ready":True}
                send_telemetry(None,None,message)

                counter=1
                battery_read_interval=60
                last_battery_read_time=0
                while True:
                    if time.time()-last_battery_read_time>battery_read_interval:
                        data=thingy.battery.read()
                        print("[THINGY - APPLICATION] Battery: ", data)
                        battery_str=str(data)
                        read_value={"value":battery_str, "description":"battery charge %"}
                        send_telemetry(THINGY_CONF_SERVICE_TOPIC,BATTERY_LEVEL_CHARACTERISTIC_TOPIC,read_value)
                        last_battery_read_time=time.time()

                    if counter >= args.count and args.count!=0:
                        break
                    counter += 1

                    #BLUEPY IS NOT THREAD SAFE, ANY BLE OPERATION MUST HAPPEN IN THE MAIN THREAD!!!
                    try:
                        entry=mqtt_incoming_message_queue.popleft()
                        message_handler(entry["payload"],entry["topic"])
                    except IndexError as e:
                        pass

                    try:
                        entry=syncronous_ble_operation_queue.popleft()
                        method=entry["method"]
                        data=entry["data"]
                        if method == "set_led_mode_constant":
                            thingy.ui.set_led_mode_constant(data["r"],data["g"],data["b"])
                    except IndexError as e:
                        pass
                    thingy.waitForNotifications(0.05)
            
            except BTLEDisconnectError as e:
                print("[THINGY - APPLICATION] Thingy disconnected. It should automatically reconnect...")
                message={"connected":False}
                send_telemetry(None,None,message)

    finally:
        if thingy!=None:
            thingy.ui.set_led_mode_off()
            thingy.disconnect()
        del thingy

if __name__ == "__main__":
    main()
