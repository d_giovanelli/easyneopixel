from bluepy.btle import UUID, Peripheral, ADDR_TYPE_RANDOM, DefaultDelegate
import argparse
import time
import struct
import binascii
import paho.mqtt.client as mqtt
import json
import time
import uuid
import traceback

MQTT_BROKER_DEF="i3app-rabbit.fbk.eu" #"iot.smartcommunitylab.it"
MQTT_PORT_DEF=1883
DEVICE_TOKEN_DEF="test_vhost:i3usr"  #"MnAZcwSpqG3XjGO4RxJy" #FIRST 4 READERS PROTOTYPE: "0alJMWpXlOcY8bZuWxvK", SECOND 4 READERS PROTOTYPE: "B43vCbweCWihtUCn6vln", BLUETOOTH PROTOTYPE: MnAZcwSpqG3XjGO4RxJy
DEVICE_PASSWORD_DEF="aaa" #"None"
DEVICE_READABLE_NAME="Thingy-Alex"

mqtt_broker=MQTT_BROKER_DEF
mqtt_port=MQTT_PORT_DEF
device_token=DEVICE_TOKEN_DEF
device_password=DEVICE_PASSWORD_DEF #shall this be added to command line arguments?

# MQTT topics
#ROOT TOPICS
TELEMETRY_TOPIC="telemetry"
RPC_REQUEST_TOPIC="rpc/request"
RPC_RESPONSE_TOPIC="rpc/response"

#DEVICE TOPIC
THINGY_TOPIC="" #the BD address (MAC address) will be put here as soon as the program is started

#BLE SERVICES TOPIC
ENVIRONMENT_SERVICE_TOPIC="environment"
UI_SERVICE_TOPIC="ui"
MOTION_SERVICE_TOPIC="motion"
SOUND_SERVICE_TOPIC="sound"
THINGY_CONF_SERVICE_TOPIC="thingy_configuration"

#BLE CHARACTERISTIC TOPIC
TEMPERATURE_CHARACTERISTIC_TOPIC="temperature"
PRESSURE_CHARACTERISTIC_TOPIC="pressure"
HUMIDITY_CHARACTERISTIC_TOPIC="humidity"
GAS_CHARACTERISTIC_TOPIC="gas"
COLOR_CHARACTERISTIC_TOPIC="color"
BUTTON_CHARACTERISTIC_TOPIC="button"
LED_CHARACTERISTIC_TOPIC="led"
TAP_CHARACTERISTIC_TOPIC="tap"
ORIENTATION_CHARACTERISTIC_TOPIC="orientation"
QUATERNION_CHARACTERISTIC_TOPIC="quaternion"
STEPCOUNTER_CHARACTERISTIC_TOPIC="step_counter"
RAWDATA_CHARACTERISTIC_TOPIC="rawdata"
EULER_CHARACTERISTIC_TOPIC="euler"
ROTATION_CHARACTERISTIC_TOPIC="rotation_matrix"
HEADING_CHARACTERISTIC_TOPIC="heading"
GRAVITY_CHARACTERISTIC_TOPIC="gravity_vector"
SPEAKER_CHARACTERISTIC_TOPIC="speaker_status"
BATTERY_LEVEL_CHARACTERISTIC_TOPIC="battery"


def send_telemetry(sensorClass,sensorType,reading):
    global mqtt_client
    global DEVICE_ID
    global THINGY_TOPIC

    topic=TELEMETRY_TOPIC+'/'+THINGY_TOPIC
    if sensorClass != None:
        topic=topic+'/'+sensorClass
    else:        
        sensorClass=""

    if sensorType != None:
        topic=topic+'/'+sensorType
    else:
        sensorType=""

    telemetry_uuid=str(uuid.uuid4())

    msg={"sensorClass":sensorClass, "sensorType":sensorType, "reading":reading, "sourceName":DEVICE_READABLE_NAME, "sourceUID":DEVICE_ID, "telemetryID":telemetry_uuid}
    
    publish_mqtt(mqtt_client,msg,topic)





def connect_mqtt():

    print("[MQTT] Connecting...")
    m_client=mqtt.Client()
    m_client.on_publish=on_publish_cb
    m_client.on_connect=on_connect_cb
    m_client.on_message=on_message_cb
    m_client.on_disconnect=on_disconnect_cb

    m_client.username_pw_set(device_token, device_password)
    m_client.connect(mqtt_broker, mqtt_port)
    return m_client

#MQTT CALLBACKS
def on_publish_cb(client, data, result):
    print("[MQTT] on_publish. data: "+ str(data) +", result: "+str(result))
    pass

def on_disconnect_cb(client, userdata, result):
    global mqtt_connection_active
    #print("[MQTT] on_disconnect_cb.")
    mqtt_connection_active=False
    pass

# The callback for when the client receives a CONNACK response from the server.
def on_connect_cb(client, userdata, flags, rc):
    global mqtt_connection_active

    if rc==0:
        mqtt_connection_active=True #TODO: This doesn't change value, maybe threads problem
    else:
        mqtt_connection_active=False

    print("[MQTT] on connect result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.  

    subscribe_topic=RPC_RESPONSE_TOPIC+'/'+THINGY_TOPIC+"/#"  #RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC+"/#"
    print("[MQTT] Subscribing to topic: "+subscribe_topic)
    client.subscribe(subscribe_topic)

    subscribe_topic=TELEMETRY_TOPIC+'/'+THINGY_TOPIC+"/#"  #RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC+"/#"
    print("[MQTT] Subscribing to topic: "+subscribe_topic)
    client.subscribe(subscribe_topic)

def publish_mqtt(mqtt_client,data_to_publish,topic=None):
    global mqtt_connection_active

    qos=1
    try:
        if topic==None:
            topic=TELEMETRY_TOPIC+'/'+THINGY_TOPIC
        print("[MQTT] Publishing this data: "+json.dumps(data_to_publish)+" on topic: "+topic)
        ret=mqtt_client.publish(topic,json.dumps(data_to_publish),qos)
        print("[MQTT] Publish return code: "+str(ret.rc))
        return ret
    except Exception as e:
        print("[MQTT] Exception in publish_mqtt!: "+ str(e))

def on_message_cb(client, userdata, msg):
    try:
        dec_message=msg.payload.decode("utf-8")
        message_handler(dec_message,msg.topic)
    except Exception as e:
        print("[MQTT] Exception in on_message!: "+ str(e))
        traceback.print_exc()

   

def message_handler(msg,topic):
    try:
        jobj=json.loads(msg)
        
        print("[MQTT] message on topic: "+ topic + " msg: "+str(jobj))

    except Exception as e:
        #with print_lock:
        print("[MQTT] Exception in multireader_message_handler!: "+ str(e))
        traceback.print_exc()
        pass

def send_configuration(device_id,service,characteristic,method,params):
    global THINGY_TOPIC
    global DEVICE_ID

    #service=UI_SERVICE_TOPIC
    #characteristic=BUTTON_CHARACTERISTIC_TOPIC
    #method="enable"
    #params=1000
    requestID=str(uuid.uuid4())
    topic=RPC_REQUEST_TOPIC+'/'+THINGY_TOPIC+'/'+service+'/'+characteristic

    request={"method": method, "params": params, "messageID": requestID, "path": service+'/'+characteristic}
    messageInfo=publish_mqtt(mqtt_client,request,topic)
    messageInfo.wait_for_publish()

def main():
    global mqtt_client
    global THINGY_TOPIC
    global DEVICE_ID

    #DEVICE_ID="e9:5e:71:f3:0c:8e"
    #DEVICE_ID="e1:9b:07:10:ac:14"
    DEVICE_ID="cf:4f:7e:f0:23:17"

    THINGY_TOPIC=DEVICE_ID+"-thingy"

    mqtt_client=None
    while mqtt_client==None:
        try:
            mqtt_client=connect_mqtt()
        except Exception as e:
            print("[MQTT] Exception during mqtt connect. " + str(e))
            time.sleep(1)

    try:
        mqtt_client.loop_start()
    except Exception as e:
        print("[MQTT] Exception during mqtt loop start. " + str(e))
    
    #send_configuration(DEVICE_ID,UI_SERVICE_TOPIC,BUTTON_CHARACTERISTIC_TOPIC,"enable",1000)
    send_configuration(DEVICE_ID,MOTION_SERVICE_TOPIC,EULER_CHARACTERISTIC_TOPIC,"enable",True)
    
    try:
        while True:
            time.sleep(0.1)
    finally:
        send_configuration(DEVICE_ID,MOTION_SERVICE_TOPIC,EULER_CHARACTERISTIC_TOPIC,"enable",False)

        mqtt_client.disconnect()

if __name__ == "__main__":
    main()
