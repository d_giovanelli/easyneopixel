#!/bin/bash

#obtain script location (it might be launched from a different one)
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

#navigate to the location of program
cd $SCRIPTPATH

#launch program
sudo python3 main.py

exit 0
