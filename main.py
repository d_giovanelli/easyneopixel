import pexpect
import time
import threading
import traceback
import signal
from datetime import datetime
################################################  USER INPUT THREAD  #############################################

class USER_INPUT_THREAD(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID= threadID
        self.name = name
        self.__loop=True

    def run(self):
        while self.__loop:
            try:
                input_str = input()
                send_input(input_str) #redirect the input to all modules
            except Exception as e:
                print("[MAIN] Read failed. "+ str(e))
                pass

################################################  HELPERS  #############################################

def send_input(self,input_str):
    global processes

    for p in processes:
        p.sendline(input_str)

################################################  START MODULES  #############################################

MODULES_DEF=list([
    ["beaglebone","sudo python3 main.py"], #the first element is the cwd - (relative) current working directory, the second the actual command
    ["thingy52","python3 main.py --address E9:5E:71:F3:0C:8E --name Thingy_1"],
    ["thingy52","python3 main.py --address E1:9B:07:10:AC:14 --name Thingy_2"],
    ])

processes=list()

user_input_thread=USER_INPUT_THREAD(1,"user input thread")
user_input_thread.setDaemon(True)
user_input_thread.start()

for m in MODULES_DEF:
    try:
        p=None
        p=pexpect.spawnu(m[1],cwd=m[0])
        processes.append(p)
    except pexpect.exceptions.EOF:
        print("A module crashed during boot..")

print("All modules started!")

loop=True
try:
    while loop:
        for i in range(len(processes)):
            try:
                processes[i].expect('\n',timeout=0)
                t_str=datetime.now().strftime("%H:%M:%S.%f")
                proc_data=processes[i].before
                print(t_str+" [MODULE "+str(i)+"] : " + proc_data)
                time.sleep(0.01)
            except pexpect.exceptions.TIMEOUT:
                pass
            except pexpect.exceptions.EOF:
                #print("[MODULE "+str(i)+"] closed. Closing everithing")
                #loop=False
                pass

finally:
    for i in range(len(processes)):
        try:
            processes[i].kill(signal.CTRL_C_EVENT)
        except Exception as e:
            pass
        time.sleep(0.1) #don't know how long it takes to kill, reserve some time then go on
