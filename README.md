NFC tangible interfaces.


This project contains the sources for turning a linux pc into an mqtt input/output device to be used in projects in the field of tangible interfaces.

The native architecture for this project is the BeagleBoneBlack Wireless, however it can be ported to any linux pc (some part work also on other OS but the purpose is not to make it multiplatform). Some functionalities are specific of the BeagleBone (such as GPIO/PRUs) then to make them available on other platforms (such as the raspberry pi) some porting is required.

The general flow is such the inputs (buttons, nfc) are pushed to an MQTT server, the same server is used to receive commands for the outputs (LEDs, sound ecc).

The entry point for the project is the "startup.sh" script in the root of the repository, once executed it manages the nfc readers, the buttons, the leds and the communication with the MQTT broker. The configurations for now are hardcoded into multireader/test/main.py.
Please note that some external libraries are required by python3. To install them:
 python3 -m pip install <library_name>

Here a list of the used libraries (might not be complete...)
- pyserial
- paho-mqtt
- pexpect
- Adafruit_BBIO

Please note that for accessing the PRU the script needs the root access, therefore the startup.sh will ask the password.


At present, the repository structure is not 100% appropriate and it should be refactored, however for now this is not a priority.
At present the structure is as follow:
doc/ : contains basic documentation (for now only the BEAGLEBONEBLACK pinout)
gpio/ : contains the code examples to control the GPIO (input/output/PWM)
mqtt/ : contains the code examples to interract with an MQTT connection (publish/subscribe)
multireader/ : contains the main of the project. Many PN532 nfc readres are managed with a single PN532 script. N istances of this script are launched to manage N readers. This is performed by multireader/test/main.py
neopixel/ : contains the code to control the NeoPixel RGB led strips
PN532/: contains the code used to control the PN532 nfc readers

At runtime only scripts in multireader/ and PN532/ are executed, moreover to control the Neopixel also the binaries in /neopixel/bin are required
