import paho.mqtt.client as mqtt
import json


connected=False

mqtt_broker="localhost" #"iot.smartcommunitylab.it"
mqtt_port=1883
device_token="l4L4kUP9SRDQiXxAlh8r"

device_topic="v1/devices/me/telemetry"

def connect_mqtt():


    print("Connecting...")
    client=mqtt.Client()
    client.on_publish=on_publish_cb
    client.on_connect = on_connect_cb
    client.on_message = on_message_cb

    #client.username_pw_set(device_token, password=None)
    client.connect(mqtt_broker, mqtt_port)
    return client

#CALLBACKS
def on_publish_cb(client, data, result):
    print("on_publish. data: "+ str(data) +", result: "+str(result))  

# The callback for when the client receives a CONNACK response from the server.
def on_connect_cb(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    connected=True
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("v1/devices/me/rpc/request/+")

# The callback for when a PUBLISH message is received from the server.
def on_message_cb(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))


client1=connect_mqtt()
client1.loop_start()

while 1:
    try:
        #if connected:
        input_str = input("Press 1 to publish ADD, 2 to publish REMOVE:\n")
        NFCID1="deadbeef"
        if input_str=='1':
            data={"NFCID1":NFCID1,"status":"ADDED"}
            print("Publishing an ADD event...")
        elif input_str=='2':
            data={"NFCID1":NFCID1,"status":"REMOVED"}
            print("Publishing a REMOVE event...")
        else:
            print("Invalid input.")
            continue
        
        print("device_topic,json.dumps(data)="+ device_topic,json.dumps(data))
        client1.publish(device_topic,json.dumps(data))
        
    except ValueError:
        print("[USER_INPUT] Read failed. Read data: "+ input_str)
